package analizadorSemantico;

import java.util.ArrayList;
import analizadorSintactico.UnidadDeCompilacion;

/**
 * Analizador semantico que se encarga de el analisis semantico del codigo. El
 * cual consiste en analizar que las sentencias y las expresiones tengan
 * sentido.
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 *
 */
public class AnalizadoSemantico {

	private UnidadDeCompilacion unidadDeCompilacion;
	private TablaSimbolos tablaSimbolos;
	private ArrayList<String> errores;

	/**
	 * Metodo constructor
	 * 
	 * @param unidadDeCompilacion
	 */
	public AnalizadoSemantico(UnidadDeCompilacion unidadDeCompilacion) {
		this.unidadDeCompilacion = unidadDeCompilacion;
		this.errores = new ArrayList<String>();
		this.tablaSimbolos = new TablaSimbolos(errores);
	}

	/**
	 * Metodo que llama a la unidad de compilacion y llena la tabla de simbolos en
	 * base a la uc.
	 */
	public void llenarTablaSimbolos() {
		unidadDeCompilacion.llenarTablaSimbolos(tablaSimbolos);
	}

	/**
	 * Metodo para analizar la semantica
	 */
	public void analizarSemantica() {
		unidadDeCompilacion.analizarSemantica(errores, tablaSimbolos);
	}

	public String calcularTipo(String tipo1, String tipo2) {
		return "solucion";
	}

	/**
	 * @return the unidadDeCompilacion
	 */
	public UnidadDeCompilacion getUnidadDeCompilacion() {
		return unidadDeCompilacion;
	}

	/**
	 * @param unidadDeCompilacion the unidadDeCompilacion to set
	 */
	public void setUnidadDeCompilacion(UnidadDeCompilacion unidadDeCompilacion) {
		this.unidadDeCompilacion = unidadDeCompilacion;
	}

	/**
	 * @return the tablaSimbolos
	 */
	public TablaSimbolos getTablaSimbolos() {
		return tablaSimbolos;
	}

	/**
	 * @param tablaSimbolos the tablaSimbolos to set
	 */
	public void setTablaSimbolos(TablaSimbolos tablaSimbolos) {
		this.tablaSimbolos = tablaSimbolos;
	}

	/**
	 * @return the errores
	 */
	public ArrayList<String> getErrores() {
		return errores;
	}

	/**
	 * @param errores the errores to set
	 */
	public void setErrores(ArrayList<String> errores) {
		this.errores = errores;
	}

}
