package analizadorSemantico;

import java.util.ArrayList;

/**
 * Clase que representa una tabla de simbolos
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class TablaSimbolos {

	private ArrayList<Simbolo> tablaSimbolos;
	private ArrayList<String> errores;

	/**
	 * Metodo constructor
	 * 
	 * @param errores
	 */
	public TablaSimbolos(ArrayList<String> errores) {
		tablaSimbolos = new ArrayList<Simbolo>();
		this.errores = errores;
	}

	/**
	 * Metodo para agregar una variable a la tabla de simbolos
	 * 
	 * @param nombre
	 * @param tipoDato
	 * @param ambito
	 */
	public void agregarVariable(String nombre, String tipoDato, Simbolo ambito) {
		if (getSimbolo(nombre, ambito) == null) {
			tablaSimbolos.add(new Simbolo(nombre, tipoDato, ambito));
		} else {
			errores.add("La variable " + nombre + " esta repetida.");
		}
	}

	/**
	 * Metodo para agregar una funcion a la tabla de simbolos
	 * 
	 * @param nombre
	 * @param tipoDato
	 * @param parametros
	 */
	public void agregarFuncion(String nombre, String tipoDato, ArrayList<String> parametros) {
		if (getSimbolo(nombre, parametros) == null) {
			tablaSimbolos.add(new Simbolo(nombre, tipoDato, parametros));
		} else {
			errores.add("La funcion " + nombre + " esta repetida.");
		}
	}

	/**
	 * Metodo para obtener un simbolo de la tabla que no sea funcion
	 * 
	 * @param nombre
	 * @param ambito
	 * @return
	 */
	public Simbolo getSimbolo(String nombre, Simbolo ambito) {
		for (Simbolo simbolo : tablaSimbolos) {
			if (!simbolo.isEsFuncion() && simbolo.getAmbito().equals(ambito) && simbolo.getNombre().equals(nombre)) {
				return simbolo;
			}
		}
		return null;
	}

	/**
	 * Metodo para obtener un simbolo de la tabla que sea funcion
	 * 
	 * @param nombre
	 * @param parametros
	 * @return
	 */
	public Simbolo getSimbolo(String nombre, ArrayList<String> parametros) {
		for (Simbolo simbolo : tablaSimbolos) {
			if (simbolo.isEsFuncion() && simbolo.getNombre().equals(nombre) && simbolo.esIgual(parametros)) {
				return simbolo;
			}
		}
		return null;
	}

	/**
	 * Metodo para retornar un arraylist con las funciones del mismo nombre
	 * 
	 * @param nombre
	 * @return
	 */
	public ArrayList<Simbolo> getSimbolos(String nombre) {
		ArrayList<Simbolo> simbolos = new ArrayList<Simbolo>();
		for (Simbolo simbolo : tablaSimbolos) {
			if (simbolo.isEsFuncion() && simbolo.getNombre().equals(nombre)) {
				simbolos.add(simbolo);
			}
		}
		return simbolos;
	}

	@Override
	public String toString() {
		return "TablaSimbolos [tablaSimbolos=" + tablaSimbolos + ", errores=" + errores + "]";
	}

	/**
	 * @return the tablaSimbolos
	 */
	public ArrayList<Simbolo> getTablaSimbolos() {
		return tablaSimbolos;
	}

	/**
	 * @param tablaSimbolos the tablaSimbolos to set
	 */
	public void setTablaSimbolos(ArrayList<Simbolo> tablaSimbolos) {
		this.tablaSimbolos = tablaSimbolos;
	}

	/**
	 * @return the errores
	 */
	public ArrayList<String> getErrores() {
		return errores;
	}

	/**
	 * @param errores the errores to set
	 */
	public void setErrores(ArrayList<String> errores) {
		this.errores = errores;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errores == null) ? 0 : errores.hashCode());
		result = prime * result + ((tablaSimbolos == null) ? 0 : tablaSimbolos.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TablaSimbolos other = (TablaSimbolos) obj;
		if (errores == null) {
			if (other.errores != null)
				return false;
		} else if (!errores.equals(other.errores))
			return false;
		if (tablaSimbolos == null) {
			if (other.tablaSimbolos != null)
				return false;
		} else if (!tablaSimbolos.equals(other.tablaSimbolos))
			return false;
		return true;
	}

}
