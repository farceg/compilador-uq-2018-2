package analizadorSemantico;

import java.util.ArrayList;

/**
 * Clase para almacenar cada fila de la tabla de simbolos, cada fila es un
 * simbolo
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Simbolo {

	private String nombre, tipo;
	private Simbolo ambito;
	private boolean esFuncion;
	private ArrayList<String> parametros;

	/**
	 * Para guardar simbolos de tipo variable
	 * 
	 * @param nombre
	 * @param tipo
	 * @param ambito
	 */
	public Simbolo(String nombre, String tipo, Simbolo ambito) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.ambito = ambito;
		esFuncion = false;
	}

	/**
	 * Para guardar simbolos de tipo funcion
	 * 
	 * @param nombre
	 * @param tipo
	 * @param parametros
	 */
	public Simbolo(String nombre, String tipo, ArrayList<String> parametros) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.parametros = parametros;
		esFuncion = true;
	}

	/**
	 * Para guardar simbolo de tipo ambito
	 * 
	 * @param nombre
	 * @param parametros
	 */
	public Simbolo(String nombre, ArrayList<String> parametros) {
		this.nombre = nombre;
		this.parametros = parametros;
		esFuncion = false;
	}

	/**
	 * Metodo para mirar si los parametros que entran en el array son igales a los
	 * parametros del simbolo
	 * 
	 * @param params
	 * @return
	 */
	public boolean esIgual(ArrayList<String> params) {
		if (params.size() == parametros.size()) {
			for (int i = 0; i < parametros.size(); i++) {
				if (!parametros.get(i).equals(params.get(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		if (esFuncion) {
			return "Simbolo [nombre=" + nombre + ", tipo=" + tipo + ", parametros=" + parametros + "]";
		} else if (ambito != null) {
			return "Simbolo [nombre=" + nombre + ", tipo=" + tipo + ", ambito=" + ambito + "]";
		}
		return "Simbolo [nombre=" + nombre + ", parametros=" + parametros + "]";
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the ambito
	 */
	public Simbolo getAmbito() {
		return ambito;
	}

	/**
	 * @param ambito the ambito to set
	 */
	public void setAmbito(Simbolo ambito) {
		this.ambito = ambito;
	}

	/**
	 * @return the esFuncion
	 */
	public boolean isEsFuncion() {
		return esFuncion;
	}

	/**
	 * @param esFuncion the esFuncion to set
	 */
	public void setEsFuncion(boolean esFuncion) {
		this.esFuncion = esFuncion;
	}

	/**
	 * @return the parametros
	 */
	public ArrayList<String> getParametros() {
		return parametros;
	}

	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(ArrayList<String> parametros) {
		this.parametros = parametros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ambito == null) ? 0 : ambito.hashCode());
		result = prime * result + (esFuncion ? 1231 : 1237);
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((parametros == null) ? 0 : parametros.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Simbolo other = (Simbolo) obj;
		if (ambito == null) {
			if (other.ambito != null)
				return false;
		} else if (!ambito.equals(other.ambito))
			return false;
		if (esFuncion != other.esFuncion)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (parametros == null) {
			if (other.parametros != null)
				return false;
		} else if (!parametros.equals(other.parametros))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}
}
