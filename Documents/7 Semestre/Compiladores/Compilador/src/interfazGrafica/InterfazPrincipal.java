package interfazGrafica;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import analizadorLexico.AnalizadorLexico;
import analizadorSemantico.AnalizadoSemantico;
import analizadorSintactico.AnalizadorSintactico;
import javax.swing.JTabbedPane;

/**
 * Clase que crea la interfaz principal del analizador, donde el usuario puede
 * interactuar con la aplicacion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 *
 */
public class InterfazPrincipal extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JMenuBar barraMenu;
	private JPanel panelPrincipal;
	private JTextArea textoLinea, textoArea;
	private JScrollPane desplazarLinea, desplazarArea, desplazarError, desplazarToken, desplazarErrorDos,
			desplazarErrorTres;
	private int linea;
	private JTable tablaErrorLexico, tablaToken, tablaErrorSintactico;
	private JMenu menuOpciones;
	private JMenuItem menuItemAnalizar, menuItemTraducir;
	private AnalizadorLexico analizadorLexico;
	private AnalizadorSintactico as;
	private AnalizadoSemantico aSem;
	private DefaultTableModel tableModel;
	private JTree arbolVisual;
	private JTable tablaErrorSemantico;
	private JPanel panel;
	private JTabbedPane tabbedPane;

	/**
	 * Metodo constructor para inicializar los atributos de la clase y darle forma a
	 * la interfaz. En este metodo se agregan todos los componentes de la interfaz y
	 * se les asignan ciertas funciones
	 */
	public InterfazPrincipal() {

		getContentPane().setLayout(null);
		setSize(860, 622);
		setResizable(false);
		setTitle("Analizador Lexico");
		setLocationRelativeTo(null);

		linea = 1;

		barraMenu = new JMenuBar();
		setJMenuBar(barraMenu);

		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(new EmptyBorder(14, 14, 14, 14));
		setContentPane(panelPrincipal);
		panelPrincipal.setLayout(null);

		textoLinea = new JTextArea();
		textoLinea.setEditable(false);
		textoLinea.setBounds(28, 56, 21, 322);

		desplazarLinea = new JScrollPane(textoLinea);
		desplazarLinea.setBounds(29, 28, 21, 281);
		panelPrincipal.add(desplazarLinea);

		desplazarArea = new JScrollPane();
		desplazarArea.setBounds(59, 28, 505, 281);
		panelPrincipal.add(desplazarArea);

		menuOpciones = new JMenu("Opciones");
		barraMenu.add(menuOpciones);

		menuItemAnalizar = new JMenuItem("Analizar");
		menuItemAnalizar.addActionListener(this);
		menuOpciones.add(menuItemAnalizar);

		menuItemTraducir = new JMenuItem("Traducir");
		menuItemTraducir.addActionListener(this);
		menuOpciones.add(menuItemTraducir);

		// Sincroniza los scrollbar de dos areas de texto
		desplazarArea.setVerticalScrollBar(desplazarLinea.getVerticalScrollBar());

		textoArea = new JTextArea();
		desplazarArea.setViewportView(textoArea);
		textoArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				contarLineas();
			}
		});
		textoArea.setWrapStyleWord(true);
		textoArea.setText("");
		contarLineas();

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(29, 339, 535, 200);
		panelPrincipal.add(tabbedPane);

		tablaToken = new JTable();
		tablaToken.setBounds(26, 431, 744, 200);
		tablaToken.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null } },
				new String[] { "Lexema", "Categoria", "Fila", "Columna" }) {

			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		tablaToken.getColumnModel().getColumn(0).setPreferredWidth(180);
		tablaToken.getColumnModel().getColumn(1).setPreferredWidth(180);
		desplazarToken = new JScrollPane(tablaToken);
		tabbedPane.addTab("Tokens", null, desplazarToken, null);

		tablaErrorLexico = new JTable();
		tablaErrorLexico.setBounds(26, 431, 744, 200);
		tablaErrorLexico.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null } },
				new String[] { "Error", "Categoria", "Fila", "Columna" }) {

			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		tablaErrorLexico.getColumnModel().getColumn(0).setPreferredWidth(180);
		tablaErrorLexico.getColumnModel().getColumn(1).setPreferredWidth(180);
		desplazarError = new JScrollPane(tablaErrorLexico);
		tabbedPane.addTab("Errores Lexicos", null, desplazarError, null);

		tablaErrorSintactico = new JTable();
		tablaErrorSintactico.setBounds(26, 431, 744, 200);
		tablaErrorSintactico.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, { null, null, null, null }, { null, null, null, null } },
				new String[] { "Mensaje", "Fila", "Columna" }) {

			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		tablaErrorSintactico.getColumnModel().getColumn(0).setPreferredWidth(180);
		tablaErrorSintactico.getColumnModel().getColumn(1).setPreferredWidth(180);
		desplazarErrorDos = new JScrollPane(tablaErrorSintactico);
		tabbedPane.addTab("Errores Sintacticos", null, desplazarErrorDos, null);

		tablaErrorSemantico = new JTable();
		tablaErrorSemantico.setBounds(26, 431, 744, 200);
		tablaErrorSemantico
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
								{ null, null }, { null, null }, { null, null }, { null, null } },
						new String[] { "Mensaje" }) {

					private static final long serialVersionUID = 1L;
					boolean[] columnEditables = new boolean[] { false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		desplazarErrorTres = new JScrollPane(tablaErrorSemantico);
		tabbedPane.addTab("Errores Semanticos", null, desplazarErrorTres, null);

		arbolVisual = new JTree(new DefaultTreeModel(new DefaultMutableTreeNode("Arbol visual")));

		JScrollPane scrollPane = new JScrollPane(arbolVisual);
		scrollPane.setBounds(588, 28, 246, 281);
		panelPrincipal.add(scrollPane);

		panel = new JPanel();
		panel.setBounds(588, 339, 246, 194);

		Image image = new ImageIcon(this.getClass().getResource("/res/uq.jpg")).getImage();

		Image image2 = image.getScaledInstance(170, 194, Image.SCALE_DEFAULT);

		ImageIcon ic = new ImageIcon(image2);

		JLabel lbl = new JLabel(ic);
		panel.add(lbl);

		panelPrincipal.add(panel);

	}

	/**
	 * Metodo para contar las lineas que se adicionan
	 */
	public void contarLineas() {
		textoLinea.setText(String.valueOf(linea) + "\n");
		for (int i = 2; i <= textoArea.getLineCount(); i++) {
			textoLinea.setText(textoLinea.getText() + i + "\n");
		}
	}

	/**
	 * Metodo para eliminar los valores de las tablas
	 * 
	 * @param tabla
	 */
	public void eliminarValoresTabla(JTable tabla) {
		DefaultTableModel tb = (DefaultTableModel) tabla.getModel();
		int a = tabla.getRowCount() - 1;
		for (int i = a; i >= 0; i--) {
			tb.removeRow(tb.getRowCount() - 1);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		/**
		 * Analiza el texto que se guardo o que abrio
		 */
		if (e.getSource() == menuItemAnalizar && !textoArea.getText().equals("")) {
			eliminarValoresTabla(tablaErrorLexico);
			eliminarValoresTabla(tablaToken);
			eliminarValoresTabla(tablaErrorSintactico);
			eliminarValoresTabla(tablaErrorSemantico);
			for (int i = 0; i <= 3; i++) {
				tabbedPane.setBackgroundAt(i, Color.getColor(""));
			}
			analizadorLexico = new AnalizadorLexico(textoArea.getText(), tablaToken, tablaErrorLexico);
			analizadorLexico.analizar();

			as = new AnalizadorSintactico(analizadorLexico.getTablaSimbolos(), tablaErrorSintactico);
			as.analizar();
			arbolVisual.setModel(new DefaultTreeModel(as.getUnidadDeCompilacion().getArbolVisual()));

			aSem = new AnalizadoSemantico(as.getUnidadDeCompilacion());
			aSem.llenarTablaSimbolos();
			aSem.analizarSemantica();

			Set<String> set = new LinkedHashSet<>(aSem.getErrores());
			aSem.getErrores().clear();
			aSem.getErrores().addAll(set);

			tableModel = (DefaultTableModel) tablaErrorSemantico.getModel();
			tableModel.setRowCount(0);
			for (int i = 0; i < aSem.getErrores().size(); i++) {
				tableModel.addRow(new Object[] { aSem.getErrores().get(i) });
			}

			if (tablaErrorLexico.getModel().getRowCount() > 0) {
				tabbedPane.setBackgroundAt(1, Color.red);
			}
			if (tablaErrorSintactico.getModel().getRowCount() > 0) {
				tabbedPane.setBackgroundAt(2, Color.red);
			}
			if (tablaErrorSemantico.getModel().getRowCount() > 0) {
				tabbedPane.setBackgroundAt(3, Color.red);
			}

		}

		if (e.getSource() == menuItemTraducir && !aSem.equals(null) && tableModel.getRowCount() == 0) {

			guardarCodigoJava(aSem.getUnidadDeCompilacion().getJavaCode());

			try {
				String rutaJava = "C:/Program Files/Java/jdk1.8.0_181/bin/javac.exe" + " src/Principal.java";
				Process p = Runtime.getRuntime().exec(rutaJava);
				p.waitFor();
				p = Runtime.getRuntime().exec("java Principal", null, new File("src/"));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * Metodo para escribir el codigo traducido en un archivo de tipo java y de
	 * nombre Principal.java
	 * 
	 * @param code
	 */
	public void guardarCodigoJava(String code) {
		try {
			FileWriter f = new FileWriter("src/Principal.java");
			BufferedWriter b = new BufferedWriter(f);

			b.write(code);
			b.flush();
			b.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
