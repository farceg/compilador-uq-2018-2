fun void _main(..) {.
inv _met1(.20; 10; 10.),
.}
fun void _met1(. int _x; int _y; int _z.){.
cuando (. (._z?_y.) ~~ _x .){.
inv _met2(._x.),
.}
.}
fun int _met2(. int _x .){.
leer _a int,
mientras (._a �~ _x.){.
_a??,
.}
imprimir(.<valor de a : >con_a.),
imprimir (.<valor de a * x : >con(._a | _x.).),
retornar _a|_x,
.}