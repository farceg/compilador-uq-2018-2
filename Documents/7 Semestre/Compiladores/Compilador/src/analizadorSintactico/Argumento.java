package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;

/**
 * Clase de un argumento
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Argumento {
	private Token id;
	private Expresion exp;
	private Token separador;
	private Argumento argumentos;

	/**
	 * Metodo constructor de la clase cuando el argumento es un id
	 * 
	 * @param id
	 */
	public Argumento(Token id) {
		this.id = id;
	}

	/**
	 * Metodo constructor de la clase cuando el argumento es una expresion
	 * 
	 * @param Expresion
	 */
	public Argumento(Expresion exp) {
		this.exp = exp;
	}

	/**
	 * Metodo constructor de la clase, cuando el argumento tiene id separador y mas
	 * argumentos
	 * 
	 * 
	 */
	public Argumento(Token id, Token separador, Argumento argumentos) {
		this.id = id;
		this.separador = separador;
		this.argumentos = argumentos;
	}

	/**
	 * Metodo constructor de la clase, cuando el argumento tiene expresion,
	 * separador y mas argumentos
	 * 
	 * 
	 */

	public Argumento(Expresion exp, Token separador, Argumento argumentos) {
		this.exp = exp;
		this.separador = separador;
		this.argumentos = argumentos;
	}

	/**
	 * Metodo para traducir los argumentos
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "";
		if (id != null) {
			code += id.getJavaCode();
			if (separador != null && argumentos != null) {
				code += "," + argumentos.getJavaCode();
			}
			return code;
		} else if (exp != null) {
			code += exp.getJavaCode();
			if (separador != null && argumentos != null) {
				code += "," + argumentos.getJavaCode();
			}
			return code;
		}
		return code;
	}

	public DefaultMutableTreeNode getArbolVisual() {
		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);
		return nodo;
	}

	@Override
	public String toString() {
		return "Argumentos [id=" + id + ", exp=" + exp + ", separador=" + separador + ", argumentos=" + argumentos
				+ "]";
	}

	/**
	 * @return the id
	 */
	public Token getId() {
		return id;
	}

	/**
	 * @return the exp
	 */
	public Expresion getExp() {
		return exp;
	}

	/**
	 * @return the separador
	 */
	public Token getSeparador() {
		return separador;
	}

	/**
	 * @return the argumentos
	 */
	public Argumento getArgumentos() {
		return argumentos;
	}

}
