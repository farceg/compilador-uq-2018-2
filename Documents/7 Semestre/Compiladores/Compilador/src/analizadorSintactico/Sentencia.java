package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase para una sentencia
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Sentencia {

	private Condicion condicion;
	private DeclaracionDeVariable declaracionDeVariable;
	private AsignacionDeVariable asignacionDeVariable;
	private Impresion impresion;
	private Retorno retorno;
	private Leer leer;
	private Incremento incremento;
	private Decremento decremento;
	private Ciclo ciclo;
	private InvocarMetodo invocarMetodo;

	/**
	 * Metodo para traducir las sentencias a Java
	 * 
	 * @return
	 */
	public String getJavaCode() {
		if (ciclo != null)
			return ciclo.getJavaCode();
		if (declaracionDeVariable != null)
			return declaracionDeVariable.getJavaCode();
		if (asignacionDeVariable != null)
			return asignacionDeVariable.getJavaCode();
		if (invocarMetodo != null)
			return invocarMetodo.getJavaCode();
		if (retorno != null)
			return retorno.getJavaCode();
		if (condicion != null)
			return condicion.getJavaCode();
		if (impresion != null)
			return impresion.getJavaCode();
		if (leer != null)
			return leer.getJavaCode();
		if (incremento != null)
			return incremento.getJavaCode();
		if (decremento != null)
			return decremento.getJavaCode();
		return "";
	}

	/**
	 * Metodo para llenar la tabla de simbolos
	 * 
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos, Simbolo ambito) {
		if (declaracionDeVariable != null)
			declaracionDeVariable.llenarTablaSimbolos(tablaSimbolos, ambito);
		if (condicion != null)
			condicion.llenarTablaSimbolos(tablaSimbolos, ambito);
		if (ciclo != null)
			ciclo.llenarTablaSimbolos(tablaSimbolos, ambito);
		if (leer != null)
			leer.llenarTablaSimbolos(tablaSimbolos, ambito);
	}

	/**
	 * Metodo para analizar la semantica de cada sentencia
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		if (declaracionDeVariable != null)
			declaracionDeVariable.analizarSemantica(errores, tablaSimbolos, ambito);
		if (leer != null)
			leer.analizarSemantica(errores, tablaSimbolos, ambito);
		if (asignacionDeVariable != null)
			asignacionDeVariable.analizarSemantica(errores, tablaSimbolos, ambito);
		if (invocarMetodo != null)
			invocarMetodo.analizarSemantica(errores, tablaSimbolos, ambito);
		if (retorno != null)
			retorno.analizarSemantica(errores, tablaSimbolos, ambito);
		if (condicion != null)
			condicion.analizarSemantica(errores, tablaSimbolos, ambito);
		if (ciclo != null)
			ciclo.analizarSemantica(errores, tablaSimbolos, ambito);
	}

	/**
	 * Meotod constructor cuando la sentencia es una invocacion
	 * 
	 * @param invocarMetodo
	 */
	public Sentencia(InvocarMetodo invocarMetodo) {
		this.invocarMetodo = invocarMetodo;
	}

	/**
	 * Metodo constructor cuando la sentencia es un ciclo
	 * 
	 * @param ciclo
	 */
	public Sentencia(Ciclo ciclo) {
		this.ciclo = ciclo;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es una declaracion de
	 * variable
	 * 
	 * @param asignacionDeVariable
	 */
	public Sentencia(AsignacionDeVariable asignacionDeVariable) {
		this.asignacionDeVariable = asignacionDeVariable;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es una declaracion de
	 * cariable
	 * 
	 * @param declaracionDeVariable
	 */
	public Sentencia(DeclaracionDeVariable declaracionDeVariable) {
		this.declaracionDeVariable = declaracionDeVariable;
	}

	/**
	 * Metodo constructor de la clase, cuando la sentencia es un ciclo
	 * 
	 * @param condicion
	 */
	public Sentencia(Condicion condicion) {
		this.condicion = condicion;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es una impresion
	 * 
	 * @param asignacionDeVariable
	 */
	public Sentencia(Impresion impresion) {
		this.impresion = impresion;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es una impresion
	 * 
	 * @param Retorno
	 */
	public Sentencia(Retorno retorno) {
		this.retorno = retorno;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es un Leer
	 * 
	 * @param Leer
	 */
	public Sentencia(Leer leer) {
		this.leer = leer;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es un incremento
	 * 
	 * @param Incremento
	 */
	public Sentencia(Incremento incremento) {
		this.incremento = incremento;
	}

	/**
	 * Metodo constructor de la clase cuando la sentencia es un decremento
	 * 
	 * @param Decremento
	 */
	public Sentencia(Decremento decremento) {
		this.decremento = decremento;
	}

	/**
	 * Metodo que construye el arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		if (ciclo != null) {
			DefaultMutableTreeNode c = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Ciclo> ciclos = new ArrayList<Ciclo>();
			ciclos.add(ciclo);
			for (Ciclo ci : ciclos) {
				c.add(ci.getArbolVisual());
			}
			return c;
		}
		if (decremento != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Decremento> de = new ArrayList<Decremento>();
			de.add(decremento);
			for (Decremento ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (incremento != null) {
			DefaultMutableTreeNode i = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Incremento> in = new ArrayList<Incremento>();
			in.add(incremento);
			for (Incremento ci : in) {
				i.add(ci.getArbolVisual());
			}
			return i;
		}
		if (leer != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Leer> de = new ArrayList<Leer>();
			de.add(leer);
			for (Leer ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (retorno != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Retorno> de = new ArrayList<Retorno>();
			de.add(retorno);
			for (Retorno ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (impresion != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Impresion> de = new ArrayList<Impresion>();
			de.add(impresion);
			for (Impresion ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (asignacionDeVariable != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<AsignacionDeVariable> de = new ArrayList<AsignacionDeVariable>();
			de.add(asignacionDeVariable);
			for (AsignacionDeVariable ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (declaracionDeVariable != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<DeclaracionDeVariable> de = new ArrayList<DeclaracionDeVariable>();
			de.add(declaracionDeVariable);
			for (DeclaracionDeVariable ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}
		if (condicion != null) {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<Condicion> de = new ArrayList<Condicion>();
			de.add(condicion);
			for (Condicion ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		} else {
			DefaultMutableTreeNode d = new DefaultMutableTreeNode("Sentencia");
			ArrayList<InvocarMetodo> de = new ArrayList<InvocarMetodo>();
			de.add(invocarMetodo);
			for (InvocarMetodo ci : de) {
				d.add(ci.getArbolVisual());
			}
			return d;
		}

	}

	@Override
	public String toString() {
		if (ciclo != null) {
			return "Sentencia [ciclo=" + ciclo + "]";
		}
		if (decremento != null) {
			return "Sentencia [decremento=" + decremento + "]";
		}
		if (incremento != null) {
			return "Sentencia [incremento=" + incremento + "]";
		}
		if (leer != null) {
			return "Sentencia [leer=" + leer + "]";
		}
		if (retorno != null) {
			return "Sentencia [retorno=" + retorno + "]";
		}
		if (impresion != null) {
			return "Sentencia [impresion=" + impresion + "]";
		}
		if (asignacionDeVariable != null) {
			return "Sentencia [asignacion=" + asignacionDeVariable + "]";
		}
		if (declaracionDeVariable != null) {
			return "Sentencia [declaracion=" + declaracionDeVariable + "]";
		}
		if (condicion != null) {
			return "Sentencia [condicion=" + condicion + "]";
		} else {
			return "Sentencia [invocarMetodo=" + invocarMetodo + "]";
		}
	}

	/**
	 * @return the condicion
	 */
	public Condicion getCondicion() {
		return condicion;
	}

	/**
	 * @return the declaracionDeVariable
	 */
	public DeclaracionDeVariable getDeclaracionDeVariable() {
		return declaracionDeVariable;
	}

	/**
	 * @return the asignacionDeVariable
	 */
	public AsignacionDeVariable getAsignacionDeVariable() {
		return asignacionDeVariable;
	}

	/**
	 * @return the impresion
	 */
	public Impresion getImpresion() {
		return impresion;
	}

	/**
	 * @return the retorno
	 */
	public Retorno getRetorno() {
		return retorno;
	}

	/**
	 * @return the leer
	 */
	public Leer getLeer() {
		return leer;
	}

	/**
	 * @return the incremento
	 */
	public Incremento getIncremento() {
		return incremento;
	}

	/**
	 * @return the decremento
	 */
	public Decremento getDecremento() {
		return decremento;
	}

	/**
	 * @return the ciclo
	 */
	public Ciclo getCiclo() {
		return ciclo;
	}

	/**
	 * @return the invocarMetodo
	 */
	public InvocarMetodo getInvocarMetodo() {
		return invocarMetodo;
	}

	/**
	 * @param condicion the condicion to set
	 */
	public void setCondicion(Condicion condicion) {
		this.condicion = condicion;
	}

	/**
	 * @param declaracionDeVariable the declaracionDeVariable to set
	 */
	public void setDeclaracionDeVariable(DeclaracionDeVariable declaracionDeVariable) {
		this.declaracionDeVariable = declaracionDeVariable;
	}

	/**
	 * @param asignacionDeVariable the asignacionDeVariable to set
	 */
	public void setAsignacionDeVariable(AsignacionDeVariable asignacionDeVariable) {
		this.asignacionDeVariable = asignacionDeVariable;
	}

	/**
	 * @param impresion the impresion to set
	 */
	public void setImpresion(Impresion impresion) {
		this.impresion = impresion;
	}

	/**
	 * @param retorno the retorno to set
	 */
	public void setRetorno(Retorno retorno) {
		this.retorno = retorno;
	}

	/**
	 * @param leer the leer to set
	 */
	public void setLeer(Leer leer) {
		this.leer = leer;
	}

	/**
	 * @param incremento the incremento to set
	 */
	public void setIncremento(Incremento incremento) {
		this.incremento = incremento;
	}

	/**
	 * @param decremento the decremento to set
	 */
	public void setDecremento(Decremento decremento) {
		this.decremento = decremento;
	}

	/**
	 * @param ciclo the ciclo to set
	 */
	public void setCiclo(Ciclo ciclo) {
		this.ciclo = ciclo;
	}

	/**
	 * @param invocarMetodo the invocarMetodo to set
	 */
	public void setInvocarMetodo(InvocarMetodo invocarMetodo) {
		this.invocarMetodo = invocarMetodo;
	}
}
