package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;

/**
 * Clase de un termino
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Termino {

	private Token termino;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param termino
	 */
	public Termino(Token termino) {
		this.termino = termino;
	}

	public Token getTermino() {
		return termino;
	}

	public void setTermino(Token termino) {
		this.termino = termino;
	}

	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {
		return termino + "";
	}

}
