package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;

/**
 * Clase de decremento
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */

public class Decremento {

	private Token id;
	private Token decremento;
	private Token finSentencia;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param id
	 * @param incremento
	 * @param finSentencia
	 */
	public Decremento(Token id, Token decremento, Token finSentencia) {
		this.decremento = decremento;
		this.id = id;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para traducir un decremento
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = id.getJavaCode() + "--;";
		return code;
	}

	public DefaultMutableTreeNode getArbolVisual() {
		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);
		return nodo;
	}

	@Override
	public String toString() {
		return "Decremento [id=" + id + ", decremento=" + decremento + ", finSentencia=" + finSentencia + "]";
	}

}