package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;

/**
 * Clase donde se define un parametro
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Parametro {

	private Token tipoDato, id;

	/**
	 * Metodo constructor cuando se tiene mas de un parametro
	 * 
	 * @param tipoDato
	 * @param id
	 */
	public Parametro(Token tipoDato, Token id) {
		this.tipoDato = tipoDato;
		this.id = id;
	}

	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {
		return "Parametro [tipoDato=" + tipoDato + ", id=" + id + "]";
	}

	/**
	 * @return the tipoDato
	 */
	public Token getTipoDato() {
		return tipoDato;
	}

	/**
	 * @param tipoDato the tipoDato to set
	 */
	public void setTipoDato(Token tipoDato) {
		this.tipoDato = tipoDato;
	}

	/**
	 * @return the id
	 */
	public Token getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Token id) {
		this.id = id;
	}

}
