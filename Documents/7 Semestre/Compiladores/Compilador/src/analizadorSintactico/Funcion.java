package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase Funcion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Funcion {

	private Token palabraReservada, tipoDato, id, parIzq;
	private ArrayList<Parametro> parametros;
	private Token parDer, llaveIzq;
	private ArrayList<Sentencia> sentencias;
	private Token llaveDer;
	private ArrayList<String> listaParametros;

	/**
	 * Constructor cuando se tiene una lista de parametros y una lista de sentencias
	 * 
	 * @param palabraReservada
	 * @param tipoDato
	 * @param id
	 * @param parIzq
	 * @param listaParametros
	 * @param parDer
	 * @param llaveIzq
	 * @param listaSentencias
	 * @param llaveDer
	 */
	public Funcion(Token palabraReservada, Token tipoDato, Token id, Token parIzq, ArrayList<Parametro> parametros,
			Token parDer, Token llaveIzq, ArrayList<Sentencia> listaSentencias, Token llaveDer) {
		this.palabraReservada = palabraReservada;
		this.tipoDato = tipoDato;
		this.id = id;
		this.parIzq = parIzq;
		this.parametros = parametros;
		this.parDer = parDer;
		this.llaveIzq = llaveIzq;
		this.sentencias = listaSentencias;
		this.llaveDer = llaveDer;
	}

	/**
	 * Metodo para traducir las funciones
	 * 
	 * @return
	 */
	public String getJavaCode() {

		String code = "";

		if (id.getLexema().equals("_main") && tipoDato.getLexema().equals("void")) {
			code = "public static void main(String[] args) { ";
		} else {
			code = " public static " + tipoDato.getJavaCode() + " " + id.getJavaCode() + "(";

			for (int i = 0; i < parametros.size(); i++) {
				if (i == parametros.size() - 1) {
					code += parametros.get(i).getTipoDato().getJavaCode() + " "
							+ parametros.get(i).getId().getJavaCode();
				} else {
					code += parametros.get(i).getTipoDato().getJavaCode() + " "
							+ parametros.get(i).getId().getJavaCode() + ", ";
				}
			}

			code += ") {";

		}
		for (Sentencia s : sentencias) {
			code += s.getJavaCode();
		}
		return code + " }";
	}

	/**
	 * Metodo para retornar los parametros en ArrayList de tipo String
	 * 
	 * @return
	 */
	public ArrayList<String> listaParametros() {
		listaParametros = new ArrayList<String>();
		for (Parametro parametro : parametros) {
			listaParametros.add(parametro.getTipoDato().getLexema());
		}
		return listaParametros;
	}

	/**
	 * Metodo para llenar la tabla de simbolos, el cual recorre la lista de
	 * parametros y agrega cada variable a la tabla.
	 * 
	 * @param tablaSimbolos
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos) {

		for (Parametro p : parametros) {
			tablaSimbolos.agregarVariable(p.getId().getLexema(), p.getTipoDato().getLexema(),
					new Simbolo(id.getLexema(), listaParametros()));
		}

		for (Sentencia s : sentencias) {
			s.llenarTablaSimbolos(tablaSimbolos, new Simbolo(id.getLexema(), listaParametros()));
		}

	}

	/**
	 * Metodo para analizar la semantica
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos) {

		boolean b = false;

		for (Sentencia s : sentencias) {
			if (s.getRetorno() != null)
				b = true;
			s.analizarSemantica(errores, tablaSimbolos, new Simbolo(id.getLexema(), listaParametros()));
		}

		if (!tipoDato.getLexema().equals("void") && !b)
			errores.add("Debe retornar.");

	}

	/**
	 * Metodo para asignar nodos al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Funcion");
		nodo.add(new DefaultMutableTreeNode(id.getLexema()));

		DefaultMutableTreeNode params = new DefaultMutableTreeNode("Parametros");
		nodo.add(params);

		for (Parametro p : parametros) {
			params.add(p.getArbolVisual());
		}

		DefaultMutableTreeNode sens = new DefaultMutableTreeNode("Sentencias");
		nodo.add(sens);

		for (Sentencia s : sentencias) {
			sens.add(s.getArbolVisual());
		}
		return nodo;
	}

	@Override
	public String toString() {
		return "Funcion [palabraReservada=" + palabraReservada + ", tipoDato=" + tipoDato + ", id=" + id + ", parIzq="
				+ parIzq + ", parametros=" + parametros + ", parDer=" + parDer + ", llaveIzq=" + llaveIzq
				+ ", sentencias=" + sentencias + ", llaveDer=" + llaveDer + "]";
	}

	/**
	 * @return the palabraReservada
	 */
	public Token getPalabraReservada() {
		return palabraReservada;
	}

	/**
	 * @param palabraReservada the palabraReservada to set
	 */
	public void setPalabraReservada(Token palabraReservada) {
		this.palabraReservada = palabraReservada;
	}

	/**
	 * @return the tipoDato
	 */
	public Token getTipoDato() {
		return tipoDato;
	}

	/**
	 * @param tipoDato the tipoDato to set
	 */
	public void setTipoDato(Token tipoDato) {
		this.tipoDato = tipoDato;
	}

	/**
	 * @return the id
	 */
	public Token getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Token id) {
		this.id = id;
	}

	/**
	 * @return the parIzq
	 */
	public Token getParIzq() {
		return parIzq;
	}

	/**
	 * @param parIzq the parIzq to set
	 */
	public void setParIzq(Token parIzq) {
		this.parIzq = parIzq;
	}

	/**
	 * @return the parametros
	 */
	public ArrayList<Parametro> getParametros() {
		return parametros;
	}

	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(ArrayList<Parametro> parametros) {
		this.parametros = parametros;
	}

	/**
	 * @return the parDer
	 */
	public Token getParDer() {
		return parDer;
	}

	/**
	 * @param parDer the parDer to set
	 */
	public void setParDer(Token parDer) {
		this.parDer = parDer;
	}

	/**
	 * @return the llaveIzq
	 */
	public Token getLlaveIzq() {
		return llaveIzq;
	}

	/**
	 * @param llaveIzq the llaveIzq to set
	 */
	public void setLlaveIzq(Token llaveIzq) {
		this.llaveIzq = llaveIzq;
	}

	/**
	 * @return the sentencias
	 */
	public ArrayList<Sentencia> getSentencias() {
		return sentencias;
	}

	/**
	 * @param sentencias the sentencias to set
	 */
	public void setSentencias(ArrayList<Sentencia> sentencias) {
		this.sentencias = sentencias;
	}

	/**
	 * @return the llaveDer
	 */
	public Token getLlaveDer() {
		return llaveDer;
	}

	/**
	 * @param llaveDer the llaveDer to set
	 */
	public void setLlaveDer(Token llaveDer) {
		this.llaveDer = llaveDer;
	}

	/**
	 * @return the listaParametros
	 */
	public ArrayList<String> getListaParametros() {
		return listaParametros;
	}

	/**
	 * @param listaParametros the listaParametros to set
	 */
	public void setListaParametros(ArrayList<String> listaParametros) {
		this.listaParametros = listaParametros;
	}

}
