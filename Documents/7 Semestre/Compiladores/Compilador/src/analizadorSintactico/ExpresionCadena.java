package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;

/**
 * Clase de expresion cadena
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class ExpresionCadena {

	private Token cadena;
	private Token palabraReservada;
	private Expresion expresion;

	/**
	 * Metodo constructor
	 * 
	 * @param cadena
	 */
	public ExpresionCadena(Token cadena) {
		this.cadena = cadena;
	}

	/**
	 * Metodo constructor
	 * 
	 * @param cadena
	 * @param palabraReservada
	 * @param expresion
	 */
	public ExpresionCadena(Token cadena, Token palabraReservada, Expresion expresion) {
		this.cadena = cadena;
		this.palabraReservada = palabraReservada;
		this.expresion = expresion;
	}

	/**
	 * Metodo para traducir una expresion cadena
	 * 
	 * @return
	 */
	public String getJavaCode() {
		if (palabraReservada != null) {
			return cadena.getJavaCode() + "+" + expresion.getJavaCode();
		} else {
			return cadena.getJavaCode();
		}
	}

	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {
		return "ExpresionCadena [cadena=" + cadena + ", palabraReservada=" + palabraReservada + ", expresion="
				+ expresion + "]";
	}

}
