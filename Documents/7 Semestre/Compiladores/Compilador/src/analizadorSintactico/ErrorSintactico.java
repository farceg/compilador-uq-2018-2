package analizadorSintactico;

/**
 * Clase para reporatr un nuevo error sintactico
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class ErrorSintactico {

	private String mensaje;
	private int fila, columna;

	public ErrorSintactico(String mensaje, int fila, int columna) {
		super();
		this.mensaje = mensaje;
		this.fila = fila;
		this.columna = columna;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getColumna() {
		return columna;
	}

	public void setColumna(int columna) {
		this.columna = columna;
	}

	@Override
	public String toString() {
		return "ErrorSintactico [mensaje=" + mensaje + ", fila=" + fila + ", columna=" + columna + "]";
	}

}
