package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Categoria;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de una exp arit.
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class ExpresionAritmetica {

	private ExpresionAritmetica expresionAritmetica1, expresionAritmetica2;
	private Token operadorAritmetico;
	private Termino termino;

	public ExpresionAritmetica(Termino termino) {
		this.termino = termino;
	}

	public ExpresionAritmetica(Termino termino, Token operadorAritmetico, ExpresionAritmetica ea1) {
		this.termino = termino;
		this.operadorAritmetico = operadorAritmetico;
		this.expresionAritmetica1 = ea1;
	}

	public ExpresionAritmetica(ExpresionAritmetica ea1) {
		this.expresionAritmetica1 = ea1;
	}

	public ExpresionAritmetica(ExpresionAritmetica ea1, Token operadorAritmetico, ExpresionAritmetica ea2) {
		this.expresionAritmetica1 = ea1;
		this.operadorAritmetico = operadorAritmetico;
		this.expresionAritmetica2 = ea2;
	}

	/**
	 * Metodo para traducir una epresion artimetica
	 * 
	 * @return
	 */
	public String getJavaCode() {
		if (expresionAritmetica1 == null)
			return "(" + termino.getTermino().getJavaCode() + ")";
		else if (expresionAritmetica2 != null)
			return "(" + expresionAritmetica1.getJavaCode() + operadorAritmetico.getJavaCode()
					+ expresionAritmetica2.getJavaCode() + ")";
		else if (termino == null && expresionAritmetica2 == null) {

			return "(" + expresionAritmetica1.getJavaCode() + ")";
		} else
			return "(" + "(" + termino.getTermino().getJavaCode() + ")" + operadorAritmetico.getJavaCode()
					+ expresionAritmetica1.getJavaCode() + ")";
	}

	/**
	 * Metodo para analizar la semantica de una expresion aritmetica, en este caso
	 * se mira si tiene un id y si este es de tipo numero.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		if (expresionAritmetica1 != null) {
			expresionAritmetica1.analizarSemantica(errores, tablaSimbolos, ambito);
		}
		if (expresionAritmetica2 != null) {
			expresionAritmetica2.analizarSemantica(errores, tablaSimbolos, ambito);
		}
		if (termino != null) {
			if (termino.getTermino().getCategoria() == Categoria.IDENTIFICADOR_PROPIO) {
				Simbolo s = tablaSimbolos.getSimbolo(termino.getTermino().getLexema(), ambito);
				if (s != null) {
					if (!(s.getTipo().equals("int") || s.getTipo().equals("double"))) {
						errores.add("La variable " + termino.getTermino().getLexema() + " no es un numero");
					}
				} else {
					errores.add("La variable " + termino.getTermino().getLexema() + " no esta declarada.");
				}
			}
		}
	}

	/**
	 * Metodo para agregar un nodo al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {

		if (expresionAritmetica1 == null)
			return "" + termino;
		else if (expresionAritmetica2 != null)
			return "" + expresionAritmetica1 + operadorAritmetico + expresionAritmetica2;
		else if (termino == null && expresionAritmetica2 == null)
			return expresionAritmetica1 + "";
		else
			return "" + termino + operadorAritmetico + expresionAritmetica1;
	}

	/**
	 * @return the expresionAritmetica1
	 */
	public ExpresionAritmetica getExpresionAritmetica1() {
		return expresionAritmetica1;
	}

	/**
	 * @return the expresionAritmetica2
	 */
	public ExpresionAritmetica getExpresionAritmetica2() {
		return expresionAritmetica2;
	}

	/**
	 * @return the operadorAritmetico
	 */
	public Token getOperadorAritmetico() {
		return operadorAritmetico;
	}

	/**
	 * @return the termino
	 */
	public Termino getTermino() {
		return termino;
	}

}
