package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase UnidadDeCompilacion, en la cual se asignan las funciones al arbol
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class UnidadDeCompilacion {

	private ArrayList<Funcion> listaFunciones;

	/**
	 * Metodo construtor que debe recibir como parametro una lista de funciones
	 * 
	 * @param listaFunciones
	 */
	public UnidadDeCompilacion(ArrayList<Funcion> listaFunciones) {
		this.listaFunciones = listaFunciones;
	}

	/**
	 * Metodo para empezar la traduccion del codigo a java
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "import javax.swing.JOptionPane; public class Principal {";
		for (Funcion f : listaFunciones) {
			code += f.getJavaCode();
		}
		code += "}";
		return code;
	}

	/**
	 * Metodo para llenar la tabla de simbolos, el cual recorre la lista de
	 * funciones y las agrega a la tabla, a la vez que mete cada funcion va llamando
	 * el llenar tabla de cada funcion, para introducir cada sentencia de la
	 * funcion.
	 * 
	 * @param tablaSimbolos
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos) {
		for (Funcion funcion : listaFunciones) {
			tablaSimbolos.agregarFuncion(funcion.getId().getLexema(), funcion.getTipoDato().getLexema(),
					funcion.listaParametros());
			funcion.llenarTablaSimbolos(tablaSimbolos);
		}
	}

	/**
	 * Metodo para analizar la semantica, el metodo llenartabla lo que hace es
	 * agregar todos los simbolos (independientemente de que tenga error semantico).
	 * En este metodo se analizan esos simbolos de la tabla, para reportar errores.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos) {
		for (Funcion funcion : listaFunciones) {
			funcion.analizarSemantica(errores, tablaSimbolos);
		}
	}

	/**
	 * Metodo para asignar nodos al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Unidad de Compilacion");

		for (Funcion funcion : listaFunciones) {
			nodo.add(funcion.getArbolVisual());
		}
		return nodo;
	}

	@Override
	public String toString() {
		return "UnidadDeCompilacion [listaFunciones=" + listaFunciones + "]";
	}

}
