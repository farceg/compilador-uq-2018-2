package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de una declaracion de variable
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class DeclaracionDeVariable {

	private Token tipoDato, finSentencia;
	private ArrayList<Token> listaId;

	/**
	 * Metodo cosntructor de la clase
	 * 
	 * @param tipoDato
	 * @param listaId
	 * @param finSenetencia
	 */
	public DeclaracionDeVariable(Token tipoDato, ArrayList<Token> listaId, Token finSenetencia) {
		this.tipoDato = tipoDato;
		this.listaId = listaId;
		this.finSentencia = finSenetencia;
	}

	/**
	 * Metodo para traducir una declaracion de variable a Java
	 * 
	 * int _a, => int a;
	 * 
	 * int _a; _b, => int a, b;
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "";
		if (tipoDato.getLexema().equals("String")) {
			for (int i = 0; i < listaId.size(); i++) {
				if (i == listaId.size() - 1) {
					code += listaId.get(i).getJavaCode();
				} else {
					code += listaId.get(i).getJavaCode() + ", ";
				}
			}
			return "String " + code + ";";
		} else if (tipoDato.getLexema().equals("int")) {
			for (int i = 0; i < listaId.size(); i++) {
				if (i == listaId.size() - 1) {
					code += listaId.get(i).getJavaCode();
				} else {
					code += listaId.get(i).getJavaCode() + ", ";
				}
			}
			return "int " + code + ";";
		} else if (tipoDato.getLexema().equals("double")) {
			for (int i = 0; i < listaId.size(); i++) {
				if (i == listaId.size() - 1) {
					code += listaId.get(i).getJavaCode();
				} else {
					code += listaId.get(i).getJavaCode() + ", ";
				}
			}
			return "double " + code + ";";
		}
		return code;
	}

	/**
	 * Metodo para llenar la tabla de simbolos
	 * 
	 * @param tablaSimbolos
	 * @param lexema
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos, Simbolo ambito) {
		for (Token t : listaId) {
			tablaSimbolos.agregarVariable(t.getLexema(), tipoDato.getLexema(), ambito);
		}
	}

	/**
	 * Metodo para analizar la semantica
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param lexema
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
//		for (Token t : listaId) {
//			Simbolo s = tablaSimbolos.getSimbolo(t.getLexema(), ambito);
//			if (s != null) {
//				errores.add("La variable " + t.getLexema() + " esta repetida.");
//			}
//		}
	}

	/**
	 * Metodo para crear nodos en el arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {
		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);
		return nodo;
	}

	/**
	 * @return the tipoDato
	 */
	public Token getTipoDato() {
		return tipoDato;
	}

	/**
	 * @param tipoDato the tipoDato to set
	 */
	public void setTipoDato(Token tipoDato) {
		this.tipoDato = tipoDato;
	}

	/**
	 * @return the finSentencia
	 */
	public Token getFinSentencia() {
		return finSentencia;
	}

	/**
	 * @param finSentencia the finSentencia to set
	 */
	public void setFinSentencia(Token finSentencia) {
		this.finSentencia = finSentencia;
	}

	/**
	 * @return the listaId
	 */
	public ArrayList<Token> getListaId() {
		return listaId;
	}

	/**
	 * @param listaId the listaId to set
	 */
	public void setListaId(ArrayList<Token> listaId) {
		this.listaId = listaId;
	}

	@Override
	public String toString() {
		return "DeclaracionDeVariable [tipoDato=" + tipoDato + ", listaId=" + listaId + ", finSentencia=" + finSentencia
				+ "]";
	}

}
