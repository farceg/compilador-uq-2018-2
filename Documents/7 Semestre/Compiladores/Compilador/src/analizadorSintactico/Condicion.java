package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de condicion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Condicion {

	private Token palabraReservada, parIzq;
	private ExpresionRelacional expRel;
	private Token parDer, llaveIzq;
	private ArrayList<Sentencia> sentencias;
	private Token llaveDer;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param palabraReservada
	 * @param parIzq
	 * @param expRel
	 * @param parDer
	 * @param llaveIzq
	 * @param sentencias
	 * @param llaveDer
	 */
	public Condicion(Token palabraReservada, Token parIzq, ExpresionRelacional expRel, Token parDer, Token llaveIzq,
			ArrayList<Sentencia> sentencias, Token llaveDer) {
		this.palabraReservada = palabraReservada;
		this.parIzq = parIzq;
		this.expRel = expRel;
		this.parDer = parDer;
		this.llaveIzq = llaveIzq;
		this.sentencias = sentencias;
		this.llaveDer = llaveDer;
	}

	/**
	 * Metodo para trducir una condicion a Java
	 * 
	 * cuando(.<params>.){.<sentencias>.} => if(<params>){<sentenicas>}
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "if(" + expRel.getJavaCode() + "){";
		for (Sentencia s : sentencias) {
			code += s.getJavaCode();
		}
		return code + "}";
	}

	/**
	 * Metodo para verificar la semantica de una condicion
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		expRel.analizarSemantica(errores, tablaSimbolos, ambito);
		for (Sentencia s : sentencias) {
			s.analizarSemantica(errores, tablaSimbolos, ambito);
		}
	}

	/**
	 * Metodo para analizar las sentencias dentro de una condicion
	 * 
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos, Simbolo ambito) {
		for (Sentencia s : sentencias) {
			s.llenarTablaSimbolos(tablaSimbolos, ambito);
		}
	}

	/**
	 * Metodo para agregar nodos al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Condicion");

		DefaultMutableTreeNode expresionRelacional = new DefaultMutableTreeNode(expRel);
		nodo.add(expresionRelacional);
		expresionRelacional.add(expRel.getArbolVisual());

		DefaultMutableTreeNode sens = new DefaultMutableTreeNode("Sentencias");
		nodo.add(sens);

		for (Sentencia s : sentencias) {
			sens.add(s.getArbolVisual());
		}

		return nodo;
	}

	@Override
	public String toString() {
		return "Condicion [palabraReservada=" + palabraReservada + ", parIzq=" + parIzq + ", expRel=" + expRel
				+ ", parDer=" + parDer + ", llaveIzq=" + llaveIzq + ", sentencias=" + sentencias + ", llaveDer="
				+ llaveDer + "]";
	}

}
