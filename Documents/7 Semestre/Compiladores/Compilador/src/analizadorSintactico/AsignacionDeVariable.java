package analizadorSintactico;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Categoria;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de una asignacion de variable, la cual consiste en un id, un operador
 * de asignacion, un termino que se asigna y un fin de sentencia.
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class AsignacionDeVariable {

	private Token id, opAsignacion;
	private Termino termino;
	private Token finSentencia;

	/**
	 * Metodo constructor
	 * 
	 * @param id
	 * @param opAsignacion
	 * @param termino
	 * @param finSentencia
	 */
	public AsignacionDeVariable(Token id, Token opAsignacion, Termino termino, Token finSentencia) {
		this.id = id;
		this.opAsignacion = opAsignacion;
		this.termino = termino;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para traducir una asignacion de variable a Java
	 * 
	 * cuando la variable es int _a = 5, => a = 5; _a ?= 5, => a += 5;
	 * 
	 * cuando la variable es String _a = <hola>, => a = "hola";
	 * 
	 * @return
	 */
	public String getJavaCode() {
		return id.getJavaCode() + " " + opAsignacion.getJavaCode() + " " + termino.getTermino().getJavaCode() + ";";
	}

	/**
	 * Metodo para verificar la correcta semantica de la asignacion de variable.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {

		Simbolo s = tablaSimbolos.getSimbolo(id.getLexema(), ambito);
		if (s == null) {
			errores.add("La variable " + id.getLexema() + " no esta declarada.");
		} else {

			if (termino.getTermino().getCategoria() == Categoria.IDENTIFICADOR_PROPIO) {
				Simbolo simbolo = tablaSimbolos.getSimbolo(termino.getTermino().getLexema(), ambito);
				if (simbolo == null) {
					errores.add("La variable " + termino.getTermino().getLexema() + " no esta declarada.");
				} else {
					if (!s.getTipo().equals(simbolo.getTipo())) {
						errores.add("La variable " + termino.getTermino().getLexema() + " no es del mismo tipo que "
								+ id.getLexema() + ".");
					}
				}
			} else if ((s.getTipo().equals("int") || s.getTipo().equals("double"))
					&& termino.getTermino().getCategoria() != Categoria.ENTERO) {
				errores.add("La variable " + id.getLexema() + " debe ser un entero.");
			} else if (s.getTipo().equals("String")
					&& termino.getTermino().getCategoria() != Categoria.CADENA_CARACTERES_PROPIA) {
				errores.add("La variable " + id.getLexema() + " debe ser una cadena.");
			}
		}
	}

	public DefaultMutableTreeNode getArbolVisual() {
		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);
		return nodo;
	}

	/**
	 * @return the id
	 */
	public Token getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Token id) {
		this.id = id;
	}

	/**
	 * @return the opRelacional
	 */
	public Token getOpRelacional() {
		return opAsignacion;
	}

	/**
	 * @param opRelacional the opRelacional to set
	 */
	public void setOpRelacional(Token opRelacional) {
		this.opAsignacion = opRelacional;
	}

	/**
	 * @return the termino
	 */
	public Termino getTermino() {
		return termino;
	}

	/**
	 * @param termino the termino to set
	 */
	public void setTermino(Termino termino) {
		this.termino = termino;
	}

	/**
	 * @return the finSentencia
	 */
	public Token getFinSentencia() {
		return finSentencia;
	}

	/**
	 * @param finSentencia the finSentencia to set
	 */
	public void setFinSentencia(Token finSentencia) {
		this.finSentencia = finSentencia;
	}

	@Override
	public String toString() {
		return "AsignacionDeVariable [id=" + id + ", opRelacional=" + opAsignacion + ", termino=" + termino
				+ ", finSentencia=" + finSentencia + "]";
	}

}
