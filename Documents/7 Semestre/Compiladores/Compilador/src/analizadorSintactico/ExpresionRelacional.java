package analizadorSintactico;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de una expresion relacional
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class ExpresionRelacional {

	private Expresion expresion1;
	private Token opRelacional;
	private Expresion expresion2;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param expresion1
	 * @param opRelacional
	 * @param expresion2
	 */
	public ExpresionRelacional(Expresion expresion1, Token opRelacional, Expresion expresion2) {
		this.expresion1 = expresion1;
		this.opRelacional = opRelacional;
		this.expresion2 = expresion2;
	}

	/**
	 * Metodo para traducir una exp relacional a java
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = expresion1.getJavaCode() + opRelacional.getJavaCode() + expresion2.getJavaCode();
		return code;
	}

	/**
	 * Metodo para analizar la semantica de una expresion relacional. Verifica que
	 * una expresion se pueda comparar con la otra, pues es un error semantico
	 * decir: "cadena" > 8.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		if ((expresion1.getExpresionAritmetica() != null && expresion2.getExpresionCadena() != null)
				|| (expresion2.getExpresionAritmetica() != null && expresion1.getExpresionCadena() != null)) {
			errores.add("No se puede comprar una expresion aritmetica con una expresion cadena");
		}
		if (expresion1.getExpresionAritmetica() != null)
			expresion1.getExpresionAritmetica().analizarSemantica(errores, tablaSimbolos, ambito);
		if (expresion2.getExpresionAritmetica() != null)
			expresion2.getExpresionAritmetica().analizarSemantica(errores, tablaSimbolos, ambito);
	}

	/**
	 * Metodo para agregar un nodo al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {
		return "ExpresionRelacional [expresion1=" + expresion1 + ", opRelacional=" + opRelacional + ", expresion2="
				+ expresion2 + "]";
	}

}
