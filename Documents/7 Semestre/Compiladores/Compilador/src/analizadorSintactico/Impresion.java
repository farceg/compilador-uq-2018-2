package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;

/**
 * Clase de impresion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Impresion {

	private Token palabraReservada, parIzq;
	private Expresion exp;
	private Token parDer;
	private Token finSentencia;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param palabraReservada
	 * @param parIzq
	 * @param exp
	 * @param parDer
	 * @param finSentencia
	 */
	public Impresion(Token palabraReservada, Token parIzq, Expresion exp, Token parDer, Token finSentencia) {
		this.palabraReservada = palabraReservada;
		this.parIzq = parIzq;
		this.exp = exp;
		this.parDer = parDer;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para imprimir en una ventana
	 * 
	 * @return
	 */
	public String getJavaCode() {
		return " JOptionPane.showMessageDialog(null, " + exp.getJavaCode() + "); ";
	}

	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Imprimir");
		ArrayList<Expresion> expresiones = new ArrayList<Expresion>();
		expresiones.add(exp);
		for (Expresion e : expresiones) {
			nodo.add(e.getArbolVisual());
		}
		return nodo;
	}

	@Override
	public String toString() {
		return "Impresion [palabraReservada=" + palabraReservada + ", parIzq=" + parIzq + ", exp=" + exp + ", parDer="
				+ parDer + ", finSentencia=" + finSentencia + "]";
	}

}
