package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Clase de una expresion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Expresion {

	private ExpresionCadena expresionCadena;
	private ExpresionAritmetica expresionAritmetica;

	/**
	 * Metodo constructor cuando la expresion es una expresio cadena
	 * 
	 * @param expresionCadena
	 */
	public Expresion(ExpresionCadena expresionCadena) {
		this.expresionCadena = expresionCadena;
	}

	public Expresion(ExpresionAritmetica expresionAritmetica) {
		this.expresionAritmetica = expresionAritmetica;
	}

	/**
	 * Metodo para traducir una expresion
	 * 
	 * @return
	 */
	public String getJavaCode() {
		if (expresionCadena != null)
			return expresionCadena.getJavaCode();
		if (expresionAritmetica != null)
			return expresionAritmetica.getJavaCode();
		return "";
	}

	public DefaultMutableTreeNode getArbolVisual() {
		return new DefaultMutableTreeNode(this);
	}

	@Override
	public String toString() {
		if (expresionAritmetica != null) {
			return expresionAritmetica + "";
		}
		return expresionCadena + "";
	}

	/**
	 * @return the expresionCadena
	 */
	public ExpresionCadena getExpresionCadena() {
		return expresionCadena;
	}

	/**
	 * @return the expresionAritmetica
	 */
	public ExpresionAritmetica getExpresionAritmetica() {
		return expresionAritmetica;
	}

	/**
	 * @param expresionCadena the expresionCadena to set
	 */
	public void setExpresionCadena(ExpresionCadena expresionCadena) {
		this.expresionCadena = expresionCadena;
	}

	/**
	 * @param expresionAritmetica the expresionAritmetica to set
	 */
	public void setExpresionAritmetica(ExpresionAritmetica expresionAritmetica) {
		this.expresionAritmetica = expresionAritmetica;
	}

}
