package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import analizadorLexico.Categoria;
import analizadorLexico.Token;

/**
 * Clase principal del analizador sintactico, la cual tiene una unidad de
 * compilacion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 *
 */
public class AnalizadorSintactico {

	private ArrayList<Token> tablaSimbolos;
	private ArrayList<ErrorSintactico> tablaErrores;
	private int posicionActual;
	private Token tokenActual;
	private UnidadDeCompilacion unidadDeCompilacion;
	private DefaultTableModel tablaErrorSintactico;

	/**
	 * Constructor que recibe como parametro una tabla de simbolos, la cual contiene
	 * todos los tokens que el analizador lexico identificos
	 * 
	 * @param tablaSimbolos
	 */
	public AnalizadorSintactico(ArrayList<Token> tablaSimbolos, JTable tablaErrorSintactico) {
		this.tablaSimbolos = tablaSimbolos;
		this.tokenActual = tablaSimbolos.get(posicionActual);
		this.tablaErrores = new ArrayList<>();

		if (tablaErrorSintactico != null) {
			this.tablaErrorSintactico = (DefaultTableModel) tablaErrorSintactico.getModel();
			this.tablaErrorSintactico.setRowCount(0);
		}

	}

	/**
	 * Metodo para iniciar el analisis sintactico, verifica si la unidad de
	 * compilacion es correcta
	 */
	public void analizar() {
		this.unidadDeCompilacion = esUnidadDeCompilacion();
	}

	/**
	 * <unidadDeCompilacion> ::= <listaDeFunciones>
	 * 
	 * La unidad de compilacion es la clase sobre la que se va a programar
	 * 
	 * @return null si no hay funciones o retorna una nueva unidad de compilacion
	 *         sobre la cual se programo
	 * 
	 */
	public UnidadDeCompilacion esUnidadDeCompilacion() {

		ArrayList<Funcion> f = esListaDeFunciones();

		if (f != null) {
			return new UnidadDeCompilacion(f);
		}

		return null;

	}

	/**
	 * <listaDeFunciones> ::= <funcion> [<listaDeFunciones>]
	 * 
	 * Metodo para veriicar que hay una o varias funciones en la unidad de
	 * compilacion
	 * 
	 * @return
	 */
	public ArrayList<Funcion> esListaDeFunciones() {

		ArrayList<Funcion> funciones = new ArrayList<Funcion>();
		Funcion f = esFuncion();

		while (f != null) {
			funciones.add(f);
			f = esFuncion();
		}

		return funciones;
	}

	/**
	 * <funcion> ::= fun id "(" [<listaParametros>] ")" "{" [<listaSentencias>] "}"
	 * 
	 * @return
	 */
	public Funcion esFuncion() {

		Token fun = tokenActual;
		if (fun.getLexema().equals("fun")) {
			obtenerSiguienteToken();
			Token tipoDato = esTipoDato();
			if (tipoDato != null) {
				obtenerSiguienteToken();
				Token id = esId();
				if (id != null) {
					obtenerSiguienteToken();
					Token parIzq = tokenActual;
					if (parIzq.getCategoria() == Categoria.PARENTESIS_IZQ) {
						obtenerSiguienteToken();

						ArrayList<Parametro> parametros = esListaDeParametros();

						Token parDer = tokenActual;
						if (parDer.getCategoria() == Categoria.PARENTESIS_DER) {
							obtenerSiguienteToken();
							Token llaveIzq = tokenActual;
							if (llaveIzq.getCategoria() == Categoria.LLAVE_IZQ) {
								obtenerSiguienteToken();

								ArrayList<Sentencia> sentencias = esListaDeSentencias();

								Token llaveDer = tokenActual;
								if (llaveDer.getCategoria() == Categoria.LLAVE_DER) {
									obtenerSiguienteToken();
									return new Funcion(fun, tipoDato, id, parIzq, parametros, parDer, llaveIzq,
											sentencias, llaveDer);
								} else {
									reportarError("Falta llave derecha en la funcion", tokenActual.getFila(),
											tokenActual.getColumna());
								}
							} else {
								reportarError("Falta llave izquierda en la funcion", tokenActual.getFila(),
										tokenActual.getColumna());
							}
						} else {
							reportarError("Falta parentesis derecho en la funcion", tokenActual.getFila(),
									tokenActual.getColumna());
						}
					} else {
						reportarError("Falta parentesis izquierdo en la funcion", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Debe tener un id en la funcion", tokenActual.getFila(), tokenActual.getColumna());
				}
			} else {
				reportarError("Debe tener el tipo de dato en la funcion", tokenActual.getFila(),
						tokenActual.getColumna());
			}
		}
		return null;
	}

	/**
	 * <listaDeParametros> ::= <parametro> [";" <listaDeParametros>]
	 * 
	 * Metodo para verificar si es una lista de parametros
	 * 
	 * @return
	 */
	public ArrayList<Parametro> esListaDeParametros() {

		ArrayList<Parametro> parametros = new ArrayList<Parametro>();
		Parametro p = esParametro();

		while (p != null) {
			parametros.add(p);
			if (tokenActual.getCategoria() == Categoria.SEPARADOR_PROPIO) {
				obtenerSiguienteToken();
				p = esParametro();
				continue;
			}
			p = null;
		}

		return parametros;
	}

	/**
	 * <parametro> ::= <tipoDato> id
	 * 
	 * Metodo para verificar si es un parametro
	 * 
	 * @return
	 */
	public Parametro esParametro() {

		Token tipoDato = esTipoDato();
		if (tipoDato != null) {
			obtenerSiguienteToken();
			Token id = esId();
			if (id != null) {
				obtenerSiguienteToken();
				return new Parametro(tipoDato, id);
			} else {
				reportarError("No se encuentra el id del parametro", tokenActual.getFila(), tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <listaDeSentencias> ::= <sentencia>[<listaDeSentencias>]
	 * 
	 * Metodo para verificar si hay una lista de sentencias dentro de la funcion
	 * 
	 * @return
	 */
	public ArrayList<Sentencia> esListaDeSentencias() {

		ArrayList<Sentencia> sentencias = new ArrayList<Sentencia>();
		Sentencia sentencia = esSentencia();

		while (sentencia != null) {
			sentencias.add(sentencia);
			sentencia = esSentencia();
		}

		return sentencias;
	}

	/**
	 * <sentencia> ::= <condicion> | <declaracionDeVariable> |
	 * <asignacionDeVariable> | <impresion> | <ciclo> | <retorno> | <lectura> |
	 * <incremento> | <decremento>
	 * 
	 * @return
	 */
	public Sentencia esSentencia() {

		Condicion c = esCondicion();
		if (c != null) {
			// obtenerSiguienteToken();
			return new Sentencia(c);
		}

		DeclaracionDeVariable d = esDeclaracionDeVariable();
		if (d != null) {
			// obtenerSiguienteToken();
			return new Sentencia(d);
		}

		AsignacionDeVariable a = esAsignacionDeVariable();
		if (a != null) {
			// obtenerSiguienteToken();
			return new Sentencia(a);
		}

		Impresion b = esImpresion();
		if (b != null) {
			// obtenerSiguienteToken();
			return new Sentencia(b);
		}

		Retorno e = esRetorno();
		if (e != null) {
			// obtenerSiguienteToken();
			return new Sentencia(e);
		}

		Leer f = esLeer();
		if (f != null) {
			// obtenerSiguienteToken();
			return new Sentencia(f);
		}

		Incremento g = esIncremento();
		if (g != null) {
			// obtenerSiguienteToken();
			return new Sentencia(g);
		}

		Decremento h = esDecremento();
		if (h != null) {
			// obtenerSiguienteToken();
			return new Sentencia(h);
		}

		Ciclo ciclo = esCiclo();
		if (ciclo != null) {
			// obtenerSiguienteToken();
			return new Sentencia(ciclo);
		}

		InvocarMetodo i = esInvocarMetodo();
		if (i != null) {
			// obtenerSiguienteToken();
			return new Sentencia(i);
		}

		return null;
	}

	/**
	 * <condicion> ::= cuando "(." <expresionRelacional> ".)" "{."
	 * <listaDeSentencias>".}"
	 * 
	 * @return
	 */
	public Condicion esCondicion() {

		Token palabraReservada = tokenActual;
		if (palabraReservada.getCategoria() == Categoria.PALABRA_RESERVADA
				&& palabraReservada.getLexema().equals("cuando")) {
			obtenerSiguienteToken();
			Token parIzq = tokenActual;
			if (parIzq.getCategoria() == Categoria.PARENTESIS_IZQ) {
				obtenerSiguienteToken();

				ExpresionRelacional expRel = esExpresionRelacional();

				if (expRel != null) {
					obtenerSiguienteToken();
					Token parDer = tokenActual;
					if (parDer.getCategoria() == Categoria.PARENTESIS_DER) {
						obtenerSiguienteToken();
						Token llaveIzq = tokenActual;
						if (llaveIzq.getCategoria() == Categoria.LLAVE_IZQ) {
							obtenerSiguienteToken();

							ArrayList<Sentencia> sentencias = esListaDeSentencias();

							if (sentencias.size() > 0) {
								Token llaveDer = tokenActual;
								if (llaveDer.getCategoria() == Categoria.LLAVE_DER) {
									obtenerSiguienteToken();
									Condicion c = new Condicion(palabraReservada, parIzq, expRel, parDer, llaveIzq,
											sentencias, llaveDer);
									return c;
								} else {
									reportarError("Falta llave derecha en la condicion", tokenActual.getFila(),
											tokenActual.getColumna());
								}
							} else {
								reportarError("Falta la lista de sentencias en la condicion", tokenActual.getFila(),
										tokenActual.getColumna());
							}
						} else {
							reportarError("Falta llave izquierda en la condicion", tokenActual.getFila(),
									tokenActual.getColumna());
						}
					} else {
						reportarError("Falta parentesis derecho en la condicion", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta expresion relacional en la condicion", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta parentesis izquierdo en la condicion", tokenActual.getFila(),
						tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <expresionRelacional> ::= <expresion> opRelacional <expresion>
	 * 
	 * @return
	 */
	public ExpresionRelacional esExpresionRelacional() {

		Expresion expresion1 = esExpresion();
		if (expresion1 != null) {
			obtenerSiguienteToken();
			Token opRelacional = esOperadorRelacional();
			if (opRelacional != null) {
				obtenerSiguienteToken();
				Expresion expresion2 = esExpresion();
				if (expresion2 != null) {
					ExpresionRelacional e = new ExpresionRelacional(expresion1, opRelacional, expresion2);
					return e;
				} else {
					reportarError("Falta expresion en la expresion relacional", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta operador relacional en la expresion relacional", tokenActual.getFila(),
						tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <expresion> ::= <expresionAritmetica> | <expresionCadena>
	 * 
	 * @return
	 */
	public Expresion esExpresion() {

		ExpresionCadena expresionCadena = esExpresionCadena();
		if (expresionCadena != null) {
			Expresion e = new Expresion(expresionCadena);
			return e;
		}

		ExpresionAritmetica expAritmetica = esExpresionAritmetica();
		if (expAritmetica != null) {
			Expresion e = new Expresion(expAritmetica);
			return e;
		}

		return null;
	}

	/**
	 * <expresionAritmetica> ::= <termino> [opArit<expresionAritmetica>] |
	 * "(."<expresionAritmetica>".)"[opArit<expresionAritmetica>]
	 * 
	 * @return
	 */
	public ExpresionAritmetica esExpresionAritmetica() {

		Termino termino = esTermino();

		if (termino != null) {
			obtenerSiguienteToken();
			if (tokenActual.getCategoria() == Categoria.OPERADOR_ARITMETICO_PROPIO) {
				Token operadorAritmetico = tokenActual;
				obtenerSiguienteToken();
				ExpresionAritmetica exp = esExpresionAritmetica();
				if (exp != null) {
					ExpresionAritmetica eRetornar = new ExpresionAritmetica(termino, operadorAritmetico, exp);
					return eRetornar;
				} else {
					reportarError("Falta una expresion aritmetica", tokenActual.getFila(), tokenActual.getColumna());
				}
			} else {
				ExpresionAritmetica eRetornar = new ExpresionAritmetica(termino);
				volverUnToken();
				return eRetornar;
			}
		}

		if (tokenActual.getCategoria() == Categoria.PARENTESIS_IZQ) {
			obtenerSiguienteToken();
			ExpresionAritmetica ea = esExpresionAritmetica();
			if (ea != null) {
				obtenerSiguienteToken();
				if (tokenActual.getCategoria() == Categoria.PARENTESIS_DER) {
					obtenerSiguienteToken();
					if (tokenActual.getCategoria() == Categoria.OPERADOR_ARITMETICO_PROPIO) {
						Token operadorAritmetico = tokenActual;
						obtenerSiguienteToken();
						ExpresionAritmetica ea2 = esExpresionAritmetica();
						if (ea2 != null) {
							ExpresionAritmetica eRetornar = new ExpresionAritmetica(ea, operadorAritmetico, ea2);
							return eRetornar;
						} else {
							reportarError("Falta expresion aritmetica", tokenActual.getFila(),
									tokenActual.getColumna());
						}
					} else {
						ExpresionAritmetica eRetornar = new ExpresionAritmetica(ea);
						volverUnToken();
						return eRetornar;
					}
				} else {
					reportarError("Falta parentesis derecho en la exp.", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta expresion aritmetica", tokenActual.getFila(), tokenActual.getColumna());
			}
		}

		return null;

	}

	/**
	 * <expresionCadena> ::= <cadena> | <cadena> con <expresion>
	 * 
	 * Metodo para verificar si es una expresion cadena
	 * 
	 * @return
	 */
	public ExpresionCadena esExpresionCadena() {

		Token cadena = tokenActual;
		if (cadena.getCategoria() == Categoria.CADENA_CARACTERES_PROPIA) {
			obtenerSiguienteToken();
			if (!tokenActual.getLexema().equals("con")) {
				volverUnToken();
				return new ExpresionCadena(cadena);
			} else {
				Token con = tokenActual;
				obtenerSiguienteToken();
				Expresion ex = esExpresion();
				if (ex != null) {
					return new ExpresionCadena(cadena, con, ex);
				} else {
					reportarError("Falta la expresion", tokenActual.getFila(), tokenActual.getColumna());
				}
			}
		}

		return null;
	}

	/**
	 * Metodo para verificar si un token es un operador artiemtico propio
	 * 
	 * @return
	 */
	public Token esOperadorAritmetico() {
		Token op = tokenActual;
		if (op.getCategoria() == Categoria.OPERADOR_ARITMETICO_PROPIO) {
			return op;
		}
		return null;
	}

	/**
	 * Metodo apra verificar si es un op rel
	 * 
	 * @return
	 */
	public Token esOperadorRelacional() {
		if (tokenActual.getCategoria() == Categoria.OPERADOR_RELACIONAL) {
			return tokenActual;
		}
		return null;
	}

	/**
	 * <declaracionDeVariable> ::= <tipoDeDato> <listaIds> ","
	 * 
	 * @return
	 */
	public DeclaracionDeVariable esDeclaracionDeVariable() {

		Token tipoDato = esTipoDato();

		if (tipoDato != null) {
			obtenerSiguienteToken();
			ArrayList<Token> listaId = esListaIdRecursivo();
			if (listaId != null) {
				Token finSentencia = tokenActual;
				if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
					obtenerSiguienteToken();
					return new DeclaracionDeVariable(tipoDato, listaId, finSentencia);
				} else {
					reportarError("Falta fin de sentencia en la declaracion de la variabel", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta identificador en la declaracion de la variable", tokenActual.getFila(),
						tokenActual.getColumna());
			}

		}

		return null;
	}

	/**
	 * <asignacionDeVariable> ::= id "=" <termino> ","
	 * 
	 * @return
	 */
	public AsignacionDeVariable esAsignacionDeVariable() {

		Token id = esId();
		if (id != null) {
			obtenerSiguienteToken();
			Token opAsignacion = esOperadorDeAsignacion();
			if (opAsignacion != null) {
				obtenerSiguienteToken();
				Termino t = esTermino();
				if (t != null) {
					obtenerSiguienteToken();
					Token finSentencia = tokenActual;
					if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
						obtenerSiguienteToken();
						return new AsignacionDeVariable(id, opAsignacion, t, finSentencia);
					} else {
						reportarError("Falta fin de sentencia en la asginacion", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta el termino que asigna en la asginacion", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else if (tokenActual.getCategoria() == Categoria.INCREMENTO_PROPIO
					|| tokenActual.getCategoria() == Categoria.DECREMENTO_PROPIO) {
				volverUnToken();
				return null;
			} else {
				reportarError("Falta operador de asignacion en la asginacion", tokenActual.getFila(),
						tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * Metodo para ver si el token es un op de asignacion
	 * 
	 * @return
	 */
	public Token esOperadorDeAsignacion() {
		if (tokenActual.getCategoria() == Categoria.OPERADOR_ASIGNACION_PROPIA) {
			return tokenActual;
		}
		return null;
	}

	/**
	 * <tipoDato> ::= int |double|String
	 * 
	 * @return
	 */
	public Token esTipoDato() {
		if (tokenActual.getLexema().equals("int") || tokenActual.getLexema().equals("String")
				|| tokenActual.getLexema().equals("double") || tokenActual.getLexema().equals("void")) {
			return tokenActual;
		}
		return null;
	}

	/**
	 * <listaId> ::= id | id ";" <listaId> ","
	 * 
	 * @return
	 */
	public ArrayList<Token> esListaIdRecursivo() {
		ArrayList<Token> a = null;
		if (tokenActual.getCategoria() == Categoria.IDENTIFICADOR_PROPIO) {
			a = new ArrayList<Token>();
			a.add(tokenActual);
			obtenerSiguienteToken();
			if (tokenActual.getCategoria() == Categoria.SEPARADOR_PROPIO) {
				obtenerSiguienteToken();
				a.addAll(esListaIdRecursivo());
			}
			return a;
		}
		return null;
	}

	/**
	 * Metodo para verificar si un token es un id
	 * 
	 * @return
	 */
	public Token esId() {

		if (tokenActual.getCategoria() == Categoria.IDENTIFICADOR_PROPIO) {
			return tokenActual;
		}

		return null;
	}

	/**
	 * <incremento> ::= id "??" ","
	 * 
	 * @return Incremento
	 */
	public Decremento esDecremento() {

		Token id = esId();
		if (id != null) {
			obtenerSiguienteToken();
			Token incremento = tokenActual;
			if (incremento.getLexema().equals("^^")) {
				obtenerSiguienteToken();
				Token finSentencia = tokenActual;
				if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
					obtenerSiguienteToken();
					return new Decremento(id, incremento, finSentencia);
				} else {
					reportarError("Falta el final de sentencia en el decremento", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta el decremento ", tokenActual.getFila(), tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <leer> ::= leer id <tipoDato> ","
	 * 
	 * @return leer
	 */
	public Leer esLeer() {

		Token palabraReservada = tokenActual;

		if (palabraReservada.getCategoria() == Categoria.PALABRA_RESERVADA
				&& palabraReservada.getLexema().equals("leer")) {
			obtenerSiguienteToken();
			Token id = esId();
			if (id != null) {
				obtenerSiguienteToken();
				Token tipoDato = esTipoDato();
				if (tipoDato != null) {
					obtenerSiguienteToken();
					Token finSentencia = tokenActual;
					if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
						obtenerSiguienteToken();
						return new Leer(palabraReservada, id, tipoDato, finSentencia);
					} else {
						reportarError("Falta el final de sentencia en el leer", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta el id en el leer", tokenActual.getFila(), tokenActual.getColumna());
				}
			} else {
				reportarError("Falta el tipo de dato en el leer", tokenActual.getFila(), tokenActual.getColumna());
			}

		}

		return null;
	}

	/**
	 * <incremento> ::= id "??" ","
	 * 
	 * @return Incremento
	 */
	public Incremento esIncremento() {

		Token id = esId();
		if (id != null) {
			obtenerSiguienteToken();
			Token incremento = tokenActual;
			if (incremento.getLexema().equals("??")) {
				obtenerSiguienteToken();
				Token finSentencia = tokenActual;
				if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
					obtenerSiguienteToken();
					return new Incremento(id, incremento, finSentencia);
				} else {
					reportarError("Falta el final de sentencia en el incremento", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta el incremento ", tokenActual.getFila(), tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <retorno> ::= retornar <expresion> ","
	 * 
	 * @return retorno
	 */
	public Retorno esRetorno() {

		Token palabraReservada = tokenActual;
		if (palabraReservada.getCategoria() == Categoria.PALABRA_RESERVADA
				&& palabraReservada.getLexema().equals("retornar")) {
			obtenerSiguienteToken();
			Expresion expresion = esExpresion();
			if (expresion != null) {
				obtenerSiguienteToken();
				Token finSentencia = tokenActual;
				if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
					obtenerSiguienteToken();
					return new Retorno(palabraReservada, expresion, finSentencia);
				} else {
					reportarError("Falta el final de sentencia en el retorno", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta la expresion en el retorno", tokenActual.getFila(), tokenActual.getColumna());
			}
		}

		return null;
	}

	/**
	 * <condicion> ::= imprimir "(." <expresion> ".)" ","
	 * 
	 * @return
	 */
	public Impresion esImpresion() {

		Token palabraReservada = tokenActual;
		if (palabraReservada.getCategoria() == Categoria.PALABRA_RESERVADA
				&& palabraReservada.getLexema().equals("imprimir")) {
			obtenerSiguienteToken();
			Token parIzq = tokenActual;
			if (parIzq.getCategoria() == Categoria.PARENTESIS_IZQ) {
				obtenerSiguienteToken();
				Expresion exp = esExpresion();
				obtenerSiguienteToken();
				if (exp != null) {
					Token parDer = tokenActual;
					if (parDer.getCategoria() == Categoria.PARENTESIS_DER) {
						obtenerSiguienteToken();
						Token finSentencia = tokenActual;
						if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
							obtenerSiguienteToken();
							return new Impresion(palabraReservada, parIzq, exp, parDer, finSentencia);
						} else {

							reportarError("Falta el final de sentencia en la impresion", tokenActual.getFila(),
									tokenActual.getColumna());
						}

					} else {
						reportarError("Falta parentesis derecho en la impresion", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta expresion en la impresion", tokenActual.getFila(), tokenActual.getColumna());
				}
			} else {

				reportarError("Falta parentesis izquierdo en la impresion", tokenActual.getFila(),
						tokenActual.getColumna());
			}

		}

		return null;

	}

	/**
	 * <ciclo>::=mientras"(."<expresionRelacional> ".)""{."[<lista de sentencias>]
	 * ".}"
	 * 
	 * metodo ciclo en el analizador sintactico
	 * 
	 * @return
	 */
	public Ciclo esCiclo() {

		Token palabraReservada = tokenActual;
		if (palabraReservada.getCategoria() == Categoria.PALABRA_RESERVADA
				&& palabraReservada.getLexema().equals("mientras")) {
			obtenerSiguienteToken();
			Token parIzq = tokenActual;
			if (parIzq.getCategoria() == Categoria.PARENTESIS_IZQ) {
				obtenerSiguienteToken();
				ExpresionRelacional expRel = esExpresionRelacional();
				if (expRel != null) {
					obtenerSiguienteToken();
					Token parDer = tokenActual;
					if (parDer.getCategoria() == Categoria.PARENTESIS_DER) {
						obtenerSiguienteToken();
						Token llaveIzq = tokenActual;
						if (llaveIzq.getCategoria() == Categoria.LLAVE_IZQ) {
							obtenerSiguienteToken();
							ArrayList<Sentencia> sentencias = esListaDeSentencias();
							if (sentencias.size() >= 0) {
								Token llaveDer = tokenActual;
								if (llaveDer.getCategoria() == Categoria.LLAVE_DER) {
									obtenerSiguienteToken();
									return new Ciclo(palabraReservada, parIzq, expRel, parDer, llaveIzq, sentencias,
											llaveDer);
								} else {
									reportarError("Falta llave derecha en el ciclo", tokenActual.getFila(),
											tokenActual.getColumna());
								}
							} else {
								reportarError("Falta la lista de sentencias en el ciclo", tokenActual.getFila(),
										tokenActual.getColumna());
							}

						} else {
							reportarError("Falta llave izquierda en en el ciclo", tokenActual.getFila(),
									tokenActual.getColumna());
						}
					} else {
						reportarError("Falta parentesis derecho en el ciclo", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta expresion relacional en ciclo", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta parentesis izquierdo en el ciclo", tokenActual.getFila(),
						tokenActual.getColumna());
			}

		}

		return null;

	}

	/**
	 * <termino> ::= <numero> | <cadena> | <id>
	 * 
	 * @return
	 */
	public Termino esTermino() {

		if (tokenActual.getCategoria() == Categoria.ENTERO
				|| tokenActual.getCategoria() == Categoria.IDENTIFICADOR_PROPIO
				|| tokenActual.getCategoria() == Categoria.CADENA_CARACTERES_PROPIA) {
			return new Termino(tokenActual);
		}

		return null;
	}

	/**
	 * Metodo para cambiar de posicion y obtener el siguiente token de la tabla de
	 * simbolos
	 */
	public void obtenerSiguienteToken() {

		if (posicionActual < tablaSimbolos.size() - 1) {
			posicionActual++;
			tokenActual = tablaSimbolos.get(posicionActual);
		}

	}

	/**
	 * <Argumentos> ::= id | <Expresion> | id ";" <Argumentos> | <Expresion> ";"
	 * <Argumentos>
	 * 
	 * @return
	 */
	public ArrayList<Argumento> esListaArgumentosRecursivo() {
		ArrayList<Argumento> a = null;
		Expresion e = esExpresion();
		if (tokenActual.getCategoria() == Categoria.IDENTIFICADOR_PROPIO && e.toString().length() < 3) {
			a = new ArrayList<Argumento>();
			a.add(new Argumento(tokenActual));
			obtenerSiguienteToken();
			if (tokenActual.getCategoria() == Categoria.SEPARADOR_PROPIO) {
				obtenerSiguienteToken();
				a.addAll(esListaArgumentosRecursivo());
			}
			return a;
		}
		if (e != null) {
			a = new ArrayList<Argumento>();
			a.add(new Argumento(e));
			obtenerSiguienteToken();
			if (tokenActual.getCategoria() == Categoria.SEPARADOR_PROPIO) {
				obtenerSiguienteToken();
				a.addAll(esListaArgumentosRecursivo());
			}
			return a;
		}

		return null;
	}

	/**
	 * <invocarMetodo> ::= inv id "(." [<argumentos>] ".) ","
	 * 
	 * @return
	 */
	public InvocarMetodo esInvocarMetodo() {

		if (tokenActual.getLexema().equals("inv")) {
			Token inv = tokenActual;
			obtenerSiguienteToken();
			Token id = esId();
			if (id != null) {
				obtenerSiguienteToken();
				Token parIzq = tokenActual;
				if (parIzq.getCategoria() == Categoria.PARENTESIS_IZQ) {
					obtenerSiguienteToken();

					ArrayList<Argumento> argumentos = esListaArgumentosRecursivo();

					Token parDer = tokenActual;
					if (parDer.getCategoria() == Categoria.PARENTESIS_DER) {
						obtenerSiguienteToken();
						Token finSentencia = tokenActual;
						if (finSentencia.getCategoria() == Categoria.COMA_SENT_FIN_PROPIA) {
							obtenerSiguienteToken();
							return new InvocarMetodo(inv, id, parIzq, argumentos, parDer, finSentencia);
						} else {
							reportarError("Falta el finsentencia en invocar metodo", tokenActual.getFila(),
									tokenActual.getColumna());
						}
					} else {
						reportarError("Falta parentisis derecho en invocar metodo", tokenActual.getFila(),
								tokenActual.getColumna());
					}
				} else {
					reportarError("Falta parentisis izquierdo en invocar metodo", tokenActual.getFila(),
							tokenActual.getColumna());
				}
			} else {
				reportarError("Falta id en invocar metodo", tokenActual.getFila(), tokenActual.getColumna());
			}
		}
		return null;

	}

	/**
	 * Metodo para volver a un token anterior
	 */
	public void volverUnToken() {
		posicionActual--;
		tokenActual = tablaSimbolos.get(posicionActual);
	}

	/**
	 * Metodo para reportar un error
	 * 
	 * @param msj
	 * @param fila
	 * @param columna
	 */
	public void reportarError(String msj, int fila, int columna) {
		tablaErrores.add(new ErrorSintactico(msj, fila, columna));
		tablaErrorSintactico.addRow(new Object[] { msj, fila, columna });
	}

	public ArrayList<Token> getTablaSimbolos() {
		return tablaSimbolos;
	}

	public void setTablaSimbolos(ArrayList<Token> tablaSimbolos) {
		this.tablaSimbolos = tablaSimbolos;
	}

	public ArrayList<ErrorSintactico> getTablaErrores() {
		return tablaErrores;
	}

	public void setTablaErrores(ArrayList<ErrorSintactico> tablaErrores) {
		this.tablaErrores = tablaErrores;
	}

	public int getPosicionActual() {
		return posicionActual;
	}

	public void setPosicionActual(int posicionActual) {
		this.posicionActual = posicionActual;
	}

	public Token getTokenActual() {
		return tokenActual;
	}

	public void setTokenActual(Token tokenActual) {
		this.tokenActual = tokenActual;
	}

	public UnidadDeCompilacion getUnidadDeCompilacion() {
		return unidadDeCompilacion;
	}

	public void setUnidadDeCompilacion(UnidadDeCompilacion unidadDeCompilacion) {
		this.unidadDeCompilacion = unidadDeCompilacion;
	}

}
