package analizadorSintactico;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Categoria;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de condicion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Retorno {
	private Token palabraReservada;
	private Expresion expresion;
	private Token finSentencia;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param palabraReservada
	 * @param expresion
	 * @param finSentencia
	 */
	public Retorno(Token palabraReservada, Expresion expresion, Token finSentencia) {
		this.palabraReservada = palabraReservada;
		this.expresion = expresion;
		this.finSentencia = finSentencia;

	}

	/**
	 * Metodo para traducir un retorno en Java
	 * 
	 * retornar <loQueSea>, => return <loQueSea>;
	 * 
	 * @return
	 */
	public String getJavaCode() {
		return "return " + expresion.getJavaCode() + ";";
	}

	/**
	 * Metodo para verificar la semantica de un retorno, que lo que se retorne
	 * coincida con el tipo de retorno de la funcion y que si se retorna un id, este
	 * se encuentre en la tabla de simbolos.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		String tipoRetorno = tablaSimbolos.getSimbolo(ambito.getNombre(), ambito.getParametros()).getTipo();
		if (tipoRetorno.equals("void")) {
			errores.add("La funcion no debe retornar.");
		} else {
			if (expresion.getExpresionCadena() != null) {
				if (!tipoRetorno.equals("String")) {
					errores.add("No coincide el tipo de retorno.");
				}
			} else if (expresion.getExpresionAritmetica() != null) {
				if (expresion.getExpresionAritmetica().getExpresionAritmetica1() == null) {
					if (expresion.getExpresionAritmetica().getTermino().getTermino()
							.getCategoria() == Categoria.IDENTIFICADOR_PROPIO) {
						String id = expresion.getExpresionAritmetica().getTermino().getTermino().getLexema();
						Simbolo s = tablaSimbolos.getSimbolo(id, ambito);
						if (s == null) {
							errores.add("La variable " + id + " no existe.");
						} else if (!s.getTipo().equals(tipoRetorno)) {
							errores.add("La variable no es de tipo " + tipoRetorno + ".");
						}
					}
				}
				if (!(tipoRetorno.equals("int") || tipoRetorno.equals("double"))) {
					errores.add("No coincide el tipo de retorno.");
				}
				expresion.getExpresionAritmetica().analizarSemantica(errores, tablaSimbolos, ambito);
			}
		}
	}

	/**
	 * Metodo para incluir un nodo en el arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Retorno");
		ArrayList<Expresion> expresiones = new ArrayList<Expresion>();
		expresiones.add(expresion);
		for (Expresion e : expresiones) {
			nodo.add(e.getArbolVisual());
		}
		return nodo;
	}

	@Override
	public String toString() {
		return "Retorno [palabraReservada=" + palabraReservada + ", expresion=" + expresion + ", finSentencia="
				+ finSentencia + "]";
	}

}
