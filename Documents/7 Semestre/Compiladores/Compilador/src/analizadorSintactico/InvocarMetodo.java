package analizadorSintactico;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase Invocarmetodo,
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class InvocarMetodo {
	private Token palabraReservada;
	private Token id;
	private Token parIzq;
	private ArrayList<Argumento> argumentos;
	private Token finSentencia;

	private Token parDer;

	/**
	 * Constructor cuando se tiene unos argumentos
	 * 
	 * @param id
	 * @param
	 */
	public InvocarMetodo(Token palabraReservada, Token id, Token parIzq, ArrayList<Argumento> argumentos, Token parDer,
			Token finSentencia) {
		this.palabraReservada = palabraReservada;
		this.id = id;
		this.parIzq = parIzq;
		this.argumentos = argumentos;
		this.parDer = parDer;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para traducir una invocacion de metodo a Java
	 * 
	 * inv _met(..), => met();
	 * 
	 * inv _met(.String _a; int _b.), => met(String a, int b);
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = id.getJavaCode() + "(";
		if (argumentos != null) {
			for (int i = 0; i < argumentos.size(); i++) {
				if (i == argumentos.size() - 1) {
					code += argumentos.get(i).getJavaCode();
				} else {
					code += argumentos.get(i).getJavaCode() + ", ";
				}
			}
		}
		code += ");";
		return code;
	}

	/**
	 * Metodo para analizar la semantica de una invocacion. Debe invocar una funcion
	 * declarada en la tabla de simbolos y como argumentos debe tener algo que
	 * coincida con los parametros de la funcion.
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {

		ArrayList<Simbolo> simbolos = tablaSimbolos.getSimbolos(id.getLexema());

		if (simbolos.isEmpty()) {
			errores.add("No existe una funcion de nombre " + id.getLexema());
		} else {
			boolean b = true;
			for (int i = 0; i < simbolos.size(); i++) {
				ArrayList<String> args = new ArrayList<String>();
				if (argumentos != null && argumentos.size() == simbolos.get(i).getParametros().size()) {
					for (int j = 0; j < argumentos.size(); j++) {
						if (argumentos.get(j).getId() != null) {
							String nombre = argumentos.get(j).getId().getLexema();
							Simbolo x = tablaSimbolos.getSimbolo(nombre, ambito);
							if (x != null) {
								args.add(x.getTipo());
							} else {
								errores.add("El argumento " + nombre + " no esta en la tabla.");
							}
						} else if (argumentos.get(j).getExp().getExpresionCadena() != null) {
							args.add("String");
						} else if (argumentos.get(j).getExp().getExpresionAritmetica() != null) {
							if (simbolos.get(i).getParametros().get(j).equals("int")) {
								args.add("int");
							} else if (simbolos.get(i).getParametros().get(j).equals("double")) {
								args.add("double");
							}
							argumentos.get(j).getExp().getExpresionAritmetica().analizarSemantica(errores,
									tablaSimbolos, ambito);
						}
					}
				}

				if (!simbolos.get(i).getParametros().equals(args)) {
					b = false;
				} else {
					b = true;
				}

			}
			if (!b) {
				errores.add(
						"Los argumentos al invocar " + id.getLexema() + " no coinciden con los parametros pedidos.");
			}
		}
	}

	/**
	 * Metodo para asignar nodos al arbol
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Invocacion");
		if (argumentos != null)
			for (Argumento ci : argumentos) {
				nodo.add(ci.getArbolVisual());
			}
		return nodo;

	}

	@Override
	public String toString() {
		return "InvocarMetodo [palabraReservada=" + palabraReservada + ", id=" + id + ", parIzq=" + parIzq
				+ ", argumentos=" + argumentos + ", finSentencia=" + finSentencia + ", parDer=" + parDer + "]";
	}

}
