package analizadorSintactico;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;
import analizadorLexico.Categoria;
import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de leer
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */
public class Leer {

	private Token palabraReservada;
	private Token id;
	private Token tipoDato;
	private Token finSentencia;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param palabraReservada
	 * @param parIzq
	 * @param exp
	 * @param parDer
	 * @param finSentencia
	 */
	public Leer(Token palabraReservada, Token id, Token tipoDato, Token finSentencia) {
		this.palabraReservada = palabraReservada;
		this.id = id;
		this.tipoDato = tipoDato;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para traducir una lectur
	 * 
	 * @return
	 */
	public String getJavaCode() {
		if (tipoDato.getCategoria() == Categoria.PALABRA_RESERVADA
				&& (tipoDato.getLexema().equals("int") || tipoDato.getLexema().equals("double"))) {
			return "int " + id.getJavaCode() + " = "
					+ "Integer.parseInt(JOptionPane.showInputDialog(\"Escriba un numero\"));";
		} else if (tipoDato.getCategoria() == Categoria.PALABRA_RESERVADA && tipoDato.getLexema().equals("String")) {
			return "String " + id.getJavaCode() + " = " + "JOptionPane.showInputDialog(\"Escriba un texto\");";
		} else {
			return "";
		}
	}

	/**
	 * Metodo para analizar la semantica de un leer
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
	}

	/**
	 * Metodo para agregar el id en la tabla de simbolos
	 * 
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos, Simbolo ambito) {
		tablaSimbolos.agregarVariable(id.getLexema(), tipoDato.getLexema(), ambito);
	}

	/**
	 * Metodo para agregar un nodo
	 * 
	 * @return
	 */
	public DefaultMutableTreeNode getArbolVisual() {
		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);
		return nodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Leer [palabraReservada=" + palabraReservada + ", id=" + id + ", tipoDato=" + tipoDato
				+ ", finSentencia=" + finSentencia + "]";
	}

}
