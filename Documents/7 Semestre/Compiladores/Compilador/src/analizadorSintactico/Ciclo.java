package analizadorSintactico;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;
import analizadorSemantico.Simbolo;
import analizadorSemantico.TablaSimbolos;

/**
 * Clase de un ciclo
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */

public class Ciclo {

	private Token palabraReservada, parIzq;
	private ExpresionRelacional expRel;
	private Token parDer, llaveIzq;
	private ArrayList<Sentencia> sentencias;
	private Token llaveDer;

	/**
	 * Constructor de ciclo
	 * 
	 * @param palabraReservada es la palabra reservada que se utiliza para que
	 *                         inicialice el ciclo "mientras"
	 * @param parIzq           paraentesis de apertura para la expresion "(."
	 * @param expRel           expresion que puede ser evaluada para que de false o
	 *                         true y haga el ciclo
	 * @param parDer           parentesis de cierre de la expresion ".)"
	 * @param llaveIzq         Inicia el cuerpo del ciclo "{."
	 * @param sentencias       lineas de codigo que se van a ejecutar dentro del
	 *                         ciclo
	 * @param llaveDer         cierra el cuerpo del ciclo ".}"
	 */
	public Ciclo(Token palabraReservada, Token parIzq, ExpresionRelacional expRel, Token parDer, Token llaveIzq,
			ArrayList<Sentencia> sentencias, Token llaveDer) {
		this.palabraReservada = palabraReservada;
		this.parIzq = parIzq;
		this.expRel = expRel;
		this.parDer = parDer;
		this.llaveIzq = llaveIzq;
		this.sentencias = sentencias;
		this.llaveDer = llaveDer;
	}

	/**
	 * metodo ciclo para cuando no hay sentencias dentro del ciclo
	 * 
	 * @param palabraReservada mientras
	 * @param parIzq           apertura del inicio de la expresion relacional
	 * @param expRel           expresion realcional que permite verificar la
	 *                         continuidad del ciclo
	 * @param parDer           cierre de la expresion relacional
	 * @param llaveIzq         apertura del ciclo
	 * @param llaveDer         cierre del ciclo
	 */
	public Ciclo(Token palabraReservada, Token parIzq, ExpresionRelacional expRel, Token parDer, Token llaveIzq,
			Token llaveDer) {
		this.palabraReservada = palabraReservada;
		this.parIzq = parIzq;
		this.expRel = expRel;
		this.parDer = parDer;
		this.llaveIzq = llaveIzq;
		this.llaveDer = llaveDer;
	}

	/**
	 * Metodo para traducir un ciclo a java
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "while(" + expRel.getJavaCode() + "){";
		if (sentencias != null) {
			for (Sentencia s : sentencias) {
				code += s.getJavaCode();
			}
			code += "}";
		}
		return code;
	}

	/**
	 * Metodo para analizar la semantica de un ciclo
	 * 
	 * @param errores
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void analizarSemantica(ArrayList<String> errores, TablaSimbolos tablaSimbolos, Simbolo ambito) {
		expRel.analizarSemantica(errores, tablaSimbolos, ambito);
		if (sentencias != null) {
			for (Sentencia s : sentencias) {
				s.analizarSemantica(errores, tablaSimbolos, ambito);
			}
		}
	}

	/**
	 * Metodo para analizar las sentencias dentro de un ciclo
	 * 
	 * @param tablaSimbolos
	 * @param ambito
	 */
	public void llenarTablaSimbolos(TablaSimbolos tablaSimbolos, Simbolo ambito) {
		if (sentencias != null) {
			for (Sentencia s : sentencias) {
				s.llenarTablaSimbolos(tablaSimbolos, ambito);
			}
		}
	}

	/**
	 * permite visualizar el arbol que muestra el ciclo
	 * 
	 * @return el arbol del ciclo del arbol
	 */
	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode("Ciclo");

		DefaultMutableTreeNode expresionRelacional = new DefaultMutableTreeNode(expRel);
		nodo.add(expresionRelacional);
		expresionRelacional.add(expRel.getArbolVisual());

		DefaultMutableTreeNode sentencias = new DefaultMutableTreeNode(this.sentencias);
		nodo.add(sentencias);

		for (Sentencia sentencia : this.sentencias) {
			sentencias.add(sentencia.getArbolVisual());
		}

		return nodo;
	}

	@Override
	public String toString() {
		return "Ciclo [palabraReservada=" + palabraReservada + ", parIzq=" + parIzq + ", expRel=" + expRel + ", parDer="
				+ parDer + ", llaveIzq=" + llaveIzq + ", sentencias=" + sentencias + ", llaveDer=" + llaveDer + "]";
	}

}
