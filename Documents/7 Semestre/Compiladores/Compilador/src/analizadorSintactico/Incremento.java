package analizadorSintactico;

import javax.swing.tree.DefaultMutableTreeNode;

import analizadorLexico.Token;

/**
 * Clase de incremento
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 24/02/2019
 */

public class Incremento {

	private Token id;
	private Token incremento;
	private Token finSentencia;

	/**
	 * Metodo constructor de la clase
	 * 
	 * @param id
	 * @param incremento
	 * @param finSentencia
	 */
	public Incremento(Token id, Token incremento, Token finSentencia) {
		this.incremento = incremento;
		this.id = id;
		this.finSentencia = finSentencia;
	}

	/**
	 * Metodo para traducir un incremento a Java
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = id.getJavaCode() + "++;";
		return code;
	}

	public DefaultMutableTreeNode getArbolVisual() {

		DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(this);

		return nodo;
	}

	@Override
	public String toString() {
		return "Incremento [id=" + id + ", incremento=" + incremento + ", finSentencia=" + finSentencia + "]";
	}

}