package analizadorLexico;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Clase Analizador, en la cual se ejecuta el metodo analizar para extraer de un
 * codigo fuente sus respectivos lexemas
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 29/09/2018
 */
public class AnalizadorLexico {

	private String codigoFuente;
	private int posicionActual, columnaActual, filaActual;
	private char caracterActual, caracterFinCodigo;
	private ArrayList<Token> tablaSimbolos, tablaErrores;
	private ArrayList<String> palabrasReservadas;
	private DefaultTableModel modelToken, modelError;

	/**
	 * Metodo constructor para la inicializacion de atributos y recibir como
	 * parametro el codigo fuente
	 * 
	 * @param codigoFuente
	 */
	public AnalizadorLexico(String codigoFuente, JTable jTablaToken, JTable jTablaError) {
		this.codigoFuente = codigoFuente;
		this.palabrasReservadas = new ArrayList<String>();
		this.tablaSimbolos = new ArrayList<Token>();
		this.tablaErrores = new ArrayList<Token>();
		this.caracterActual = this.codigoFuente.charAt(0);
		this.posicionActual = 0;
		this.columnaActual = 1;
		this.filaActual = 1;
		this.caracterFinCodigo = 0;

		if (jTablaError != null && jTablaToken != null) {
			modelToken = (DefaultTableModel) jTablaToken.getModel();
			modelToken.setRowCount(0);

			modelError = (DefaultTableModel) jTablaError.getModel();
			modelError.setRowCount(0);
		}

		llenarPalabrasReservadas();

	}

	/**
	 * Metodo para analizar el codigo que entra como parametro y encontrar en este
	 * sus respectivos lexemas
	 */
	public void analizar() {

		while (caracterActual != caracterFinCodigo) {

			if (caracterActual == ' ' || caracterActual == '\n' || caracterActual == '\t' || caracterActual == '\b'
					|| caracterActual == '\f' || caracterActual == '\r') {
				obtenerSiguienteCaracter();
				continue;
			}
			if (esEntero()) {
				continue;
			}
//			if (esReal()) {
//				continue;
//			}
			if (esPalabra()) {
				continue;
			}
			if (esComentarioLineaPropio()) {
				continue;
			}
			if (esComentarioBloquePropio()) {
				continue;
			}
			if (esCadenaCaracteresPropia()) {
				continue;
			}
			if (esIdentificadorPropio()) {
				continue;
			}
			if (esArregloPropio()) {
				continue;
			}
			if (esCaracterPropio()) {
				continue;
			}
			if (esOperadorAritmeticoPropio()) {
				continue;
			}
			if (esPuntoPropio()) {
				continue;
			}
			if (esFinDeSentencia()) {

				continue;
			}
			if (esOperadorAsignacionPropia()) {
				continue;
			}
			if (esIncrementoPropio()) {
				continue;
			}
			if (esAgrupadorAbiertoPropio()) {
				continue;
			}
			if (esAgrupadorCerradoPropio()) {
				continue;
			}
			if (esSeparadorPropio()) {
				continue;
			}
			if (esDecrementoPropio()) {
				continue;
			}
			if (esOperadorLogico()) {
				continue;
			}
			if (esOperadorRelacional()) {
				continue;
			}
			almacenarError("" + caracterActual, filaActual, columnaActual);
			obtenerSiguienteCaracter();
		}

	}

	/**
	 * Metodo que encuentra un caracter dentro del codigo, en este caso un caracter
	 * puede ser asi: [a]
	 * 
	 * @return
	 */
	public boolean esCaracterPropio() {

		if (caracterActual != '[') {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != ']') {
			almacenarError(lexema, filaInicio, columnaInicio);
			return false;
		}

		lexema += caracterActual;
		almacenarSimbolo(lexema, Categoria.CARACTER_PROPIO, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return true;
	}

	/**
	 * Metodo para encontrar un arreglo en el codigo
	 * 
	 * @return
	 */
	public boolean esArregloPropio() {

		if (caracterActual != '&') {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '&') {
			if (caracterActual == '"' || Character.isDigit(caracterActual) || esLetra(caracterActual)) {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				continue;
			} else if (caracterActual == ':') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				if (caracterActual == '"' || Character.isDigit(caracterActual) || esLetra(caracterActual)) {
					continue;
				} else {
					almacenarError(lexema, filaInicio, columnaInicio);
					return false;
				}
			} else {
				return false;
			}
		}
		lexema += caracterActual;
		almacenarSimbolo(lexema, Categoria.ARREGLO_PROPIO, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return true;
	}

	/**
	 * Metodo para identificar un identificador en el codigo
	 * 
	 * @return false si no es un identificador true caso contrario
	 */
	public boolean esIdentificadorPropio() {
		if (caracterActual != '_') {
			return false;
		}
		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();
		while (Character.isDigit(caracterActual) || esLetra(caracterActual)) {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}
		almacenarSimbolo(lexema, Categoria.IDENTIFICADOR_PROPIO, filaInicio, columnaInicio);
		return true;
	}

	/**
	 * Metodo para encontrar una cadena de caracteres del lenguaje propio
	 * 
	 * @return
	 */
	public boolean esCadenaCaracteresPropia() {

		if (caracterActual != '<') {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '>') {
			if (caracterActual == caracterFinCodigo) {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}
		lexema += caracterActual;
		almacenarSimbolo(lexema, Categoria.CADENA_CARACTERES_PROPIA, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return true;
	}

	/**
	 * Metodo para encontrar un comentario de bloque, no lo almacena.
	 * 
	 * @return true si lo encuentra o false si no
	 */
	public boolean esComentarioBloquePropio() {

		if (caracterActual != '#') {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '#') {
			almacenarError(lexema, filaInicio, columnaInicio);
			return false;
		}

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '$') {
			if (caracterActual == caracterFinCodigo) {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual == '$') {
			lexema += caracterActual;
			// almacenarSimbolo(lexema, Categoria.COMENTARIO_BLOQUE_PROPIO, filaInicio,
			// columnaInicio);
			obtenerSiguienteCaracter();
			return true;
		}

		almacenarError(lexema, filaInicio, columnaInicio);
		return false;
	}

	/**
	 * Metodo para encontrar un comentario de linea, no se almacena
	 * 
	 * @return true si lo encuentra o false si no
	 */
	public boolean esComentarioLineaPropio() {

		if (caracterActual != '*') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '-') {
			hacerBacktracking(posInicio, filaInicio, columnaInicio);
			return false;
		}

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '*') {
			almacenarError(lexema, filaInicio, columnaInicio);
			obtenerSiguienteCaracter();
			return false;
		}

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '\n' && caracterActual != caracterFinCodigo) {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		// almacenarSimbolo(lexema, Categoria.COMENTARIO_LINEA_PROPIO, filaInicio,
		// columnaInicio);
		return true;
	}

	/**
	 * Metodo para verificar si en ecodigo se encuentran booleanos
	 * 
	 * @return true si encuentra o false si no
	 */
	public boolean esBoolean(String lexema) {
		if (lexema.equals("si") || lexema.equals("no")) {
			return true;
		}
		return false;
	}

	/**
	 * Metodo para llenar la lista de las palabras reservadas
	 */
	public void llenarPalabrasReservadas() {
		palabrasReservadas.add("cuando");
		palabrasReservadas.add("imprimir");
		palabrasReservadas.add("leer");
		palabrasReservadas.add("mientras");
		palabrasReservadas.add("int");
		palabrasReservadas.add("String");
		palabrasReservadas.add("double");
		palabrasReservadas.add("fun");
		palabrasReservadas.add("retornar");
		palabrasReservadas.add("con");
		palabrasReservadas.add("void");
		palabrasReservadas.add("inv");
	}

	/**
	 * Metodo para verificar si una palabra encontada es una palabra reservada
	 * 
	 * @param lexema
	 * @return
	 */
	public boolean esPalabraReservada(String lexema) {
		return palabrasReservadas.contains(lexema);
	}

	/**
	 * Metodo apra verificar si un lexema que se encuentra en el codigo fuente es un
	 * indentificador
	 * 
	 * @return
	 */
	public boolean esPalabra() {

		if (!esLetra(caracterActual)) {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (esLetra(caracterActual) || Character.isDigit(caracterActual)) {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		if (esPalabraReservada(lexema)) {
			almacenarSimbolo(lexema, Categoria.PALABRA_RESERVADA, filaInicio, columnaInicio);
			return true;
		}
		if (esBoolean(lexema)) {
			almacenarSimbolo(lexema, Categoria.BOOLEANO, filaInicio, columnaInicio);
			return true;
		}

		hacerBacktracking(posInicio, filaInicio, columnaInicio);
		return false;

	}

	/**
	 * Metodo para verificar si el token que se encuentra es un numero entero
	 * 
	 * @return
	 */
	public boolean esEntero() {

		if (!Character.isDigit(caracterActual)) {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();
		while (Character.isDigit(caracterActual)) {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		almacenarSimbolo(lexema, Categoria.ENTERO, filaInicio, columnaInicio);
		return true;
	}

	/**
	 * Metodo para encontrar un numero real en el codigo
	 * 
	 * @return false si no se encuentra que el token sea un numero real
	 */
	public boolean esReal() {

		if (Character.isDigit(caracterActual) || caracterActual == '.') {

			int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
			String lexema = "";
			lexema += caracterActual;

			if (Character.isDigit(caracterActual)) {
				obtenerSiguienteCaracter();
				while (Character.isDigit(caracterActual)) {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
				}
				if (caracterActual != '.') {
					hacerBacktracking(posInicio, filaInicio, columnaInicio);
					return false;
				}
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				while (Character.isDigit(caracterActual)) {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
				}
				almacenarSimbolo(lexema, Categoria.REAL, posInicio, columnaInicio);
				obtenerSiguienteCaracter();
				return true;
			}
			if (caracterActual == '.') {
				obtenerSiguienteCaracter();
				if (Character.isDigit(caracterActual)) {
					while (Character.isDigit(caracterActual)) {
						lexema += caracterActual;
						obtenerSiguienteCaracter();
					}
					almacenarSimbolo(lexema, Categoria.REAL, filaInicio, columnaInicio);
					obtenerSiguienteCaracter();
					return true;
				}

				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;

			}
		}
		return false;
	}

	/**
	 * Metodo para alamacenar un token en la tabla de simbolos
	 * 
	 * @param lexema    cadena de caracteres
	 * @param fila      posicionamiento de la fila donde reconoce que es un token
	 * @param columna   posicionamiento de la columna donde se reconoce que es un
	 *                  token
	 * @param categoria permite identificar el token a que categoria pertenece
	 */
	public void almacenarSimbolo(String lexema, Categoria categoria, int fila, int columna) {
		tablaSimbolos.add(new Token(lexema, categoria, fila, columna));
		//
		modelToken.addRow(new Object[] { lexema, categoria, fila, columna });
	}

	/**
	 * Metodo para alamacenar un error en la tabla de errores, siempre es de
	 * categoria ERROR
	 * 
	 * @param lexema  cadena de caracteres
	 * @param fila    posicionamiento en la fila donde se presento el fallo
	 * @param columna posicionamiento en la columna donde se presento el fallo
	 */

	public void almacenarError(String lexema, int fila, int columna) {
		tablaErrores.add(new Token(lexema, Categoria.ERROR, fila, columna));
		//
		modelError.addRow(new Object[] { lexema, Categoria.ERROR, fila, columna });
	}

	/**
	 * Metodo para regresar a un valor anterior en el codigo fuente
	 * 
	 * @param posInicio
	 */
	public void hacerBacktracking(int posInicio, int filaInicio, int columnaInicio) {
		posicionActual = posInicio;
		filaActual = filaInicio;
		columnaActual = columnaInicio;
		caracterActual = codigoFuente.charAt(posInicio);
	}

	/**
	 * Metodo para verificar si un caracter es una letra
	 * 
	 * @param a
	 * @return
	 */
	public boolean esLetra(char a) {
		if ((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z')) {
			return true;
		}
		return false;
	}

	/**
	 * Metodo para obtener el siguiente caracter en el codigo fuente, siempre y
	 * cuando exista uno.
	 */
	public void obtenerSiguienteCaracter() {
		if (posicionActual == codigoFuente.length() - 1) {
			caracterActual = caracterFinCodigo;
		} else {
			if (caracterActual == '\n') {
				filaActual++;
				columnaActual = 1;
			} else {
				columnaActual++;
			}
			posicionActual++;
			caracterActual = codigoFuente.charAt(posicionActual);
		}
	}

	/**
	 * Metodo para encontrar un operador logico en el codigo
	 * 
	 * @return
	 */
	public boolean esOperadorLogico() {

		if (caracterActual == 'y' || caracterActual == 'o' || caracterActual == 'd') {

			int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
			String lexema = "";
			lexema += caracterActual;

			if (caracterActual == 'y') {
				obtenerSiguienteCaracter();
				if (caracterActual == 'y') {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
					almacenarSimbolo(lexema, Categoria.OPERADOR_LOGICO, filaInicio, columnaInicio);
					return true;
				} else {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
					almacenarError(lexema, filaInicio, columnaInicio);
					return false;
				}
			}

			if (caracterActual == 'o') {
				obtenerSiguienteCaracter();
				if (caracterActual == 'o') {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
					almacenarSimbolo(lexema, Categoria.OPERADOR_LOGICO, filaInicio, columnaInicio);
					return true;
				} else {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
					almacenarError(lexema, filaInicio, columnaInicio);
					return false;
				}
			}

			if (caracterActual == 'd') {
				obtenerSiguienteCaracter();
				if (caracterActual == '=') {
					lexema += caracterActual;
					obtenerSiguienteCaracter();
					almacenarSimbolo(lexema, Categoria.OPERADOR_LOGICO, filaInicio, columnaInicio);
					return true;
				} else {
					hacerBacktracking(posInicio, filaInicio, columnaInicio);
					return false;
				}
			}
		}

		return false;

	}

	/**
	 * Metodo para verificar si hay un operador relacional en el codigo
	 * 
	 * @return true si encuentra un op relacional y almacena el simbolo, si no false
	 *         y hace backtracking
	 */
	public boolean esOperadorRelacional() {

		if (caracterActual != '�' && caracterActual != '�' && caracterActual != '~') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;

		if (caracterActual == '�') {
			obtenerSiguienteCaracter();
			if (caracterActual == '~') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.OPERADOR_RELACIONAL, filaInicio, columnaInicio);
				return true;
			} else {
				almacenarSimbolo(lexema, Categoria.OPERADOR_RELACIONAL, filaInicio, columnaInicio);
				return true;
			}
		}

		if (caracterActual == '�') {
			obtenerSiguienteCaracter();
			if (caracterActual == '~') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.OPERADOR_RELACIONAL, filaInicio, columnaInicio);
				return true;
			} else {
				almacenarSimbolo(lexema, Categoria.OPERADOR_RELACIONAL, filaInicio, columnaInicio);
				return true;
			}
		}

		if (caracterActual == '~') {
			obtenerSiguienteCaracter();
			if (caracterActual == '~') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.OPERADOR_RELACIONAL, filaInicio, columnaInicio);
				return true;
			} else {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			}
		}

		return false;
	}

	/**
	 * Metodo para encontrar un comentario de bloque
	 * 
	 * @return true si es un comentario de bloque, de lo contrario hace backtracking
	 *         o reporta un error
	 */
	public boolean esComentarioBloque() {

		if (caracterActual != '/') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '*') {

			if (caracterActual == '/') {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			} else {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
		}

		while (caracterActual != caracterFinCodigo) {
			if (caracterActual == '/' && codigoFuente.charAt(posicionActual - 1) == '*') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.COMENTARIO_BLOQUE, filaInicio, columnaInicio);
				return true;
			}
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		lexema += caracterActual;
		almacenarError(lexema, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return false;
	}

	/**
	 * Metodo para encontrar un comentario de bloque
	 * 
	 * @return true si es un comentario de bloque, de lo contrario hace backtracking
	 *         o reporta un error
	 */
	public boolean esComentarioBloque2() {

		if (caracterActual != '/') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '*') {
			if (caracterActual == '/') {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			} else {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
		}

		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '/' && codigoFuente.charAt(posicionActual - 1) != '*') {
			if (caracterActual == caracterFinCodigo) {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
			lexema += caracterActual;
			obtenerSiguienteCaracter();
		}

		lexema += caracterActual;
		almacenarSimbolo(lexema, Categoria.COMENTARIO_BLOQUE, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return true;
	}

	/**
	 * Metodo para verificar si un lexema es de tipo comentario de linea
	 * 
	 * @return
	 */
	public boolean esComentarioLinea() {

		if (caracterActual != '/') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual != '/') {

			if (caracterActual == '*') {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			} else {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
		}

		while (caracterActual != '\n' && caracterActual != caracterFinCodigo) {
			obtenerSiguienteCaracter();
		}

		return true;

	}

	/**
	 * Metodo para encontrar una cadena de caracteres en el codigo fuente
	 * 
	 * @return
	 */
	public boolean esCadenaCaracteres() {

		if (caracterActual != '"') {
			return false;
		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		while (caracterActual != '"' && caracterActual != caracterFinCodigo) {

			lexema += caracterActual;
			obtenerSiguienteCaracter();

			if (caracterActual == 92) {

				lexema += caracterActual;
				obtenerSiguienteCaracter();

				//
				if (!(codigoFuente.charAt(posicionActual) == 98 || codigoFuente.charAt(posicionActual) == 116
						|| codigoFuente.charAt(posicionActual) == 110 || codigoFuente.charAt(posicionActual) == 102
						|| codigoFuente.charAt(posicionActual) == 114 || codigoFuente.charAt(posicionActual) == 34
						|| codigoFuente.charAt(posicionActual) == 39 || codigoFuente.charAt(posicionActual) == 92)) {
					almacenarError(lexema, filaInicio, columnaInicio);
				}

			}

		}

		if (caracterActual == caracterFinCodigo) {
			almacenarError(lexema, filaInicio, columnaInicio);
		} else {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
			almacenarSimbolo(lexema, Categoria.CADENA_CARACTERES, filaInicio, columnaInicio);
		}

		return true;
	}

	/**
	 * Metodo para verificar si el caracter actual es un operador aritmetico o no
	 * 
	 * @return true si es un operador aritmetico, false caso contrario
	 */
	public boolean esOperadorAritmeticoPropio() {

		if (!(caracterActual == '?' || caracterActual == '�' || caracterActual == '|' || caracterActual == '!'
				|| caracterActual == '�')) {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;

		if ((caracterActual == '?' || caracterActual == '�' || caracterActual == '|' || caracterActual == '!'
				|| caracterActual == '�')) {

			obtenerSiguienteCaracter();

			if (caracterActual == '?' || caracterActual == '�' || caracterActual == '=') {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			}
			almacenarSimbolo(lexema, Categoria.OPERADOR_ARITMETICO_PROPIO, filaInicio, columnaInicio);
			return true;
		}

		hacerBacktracking(posInicio, filaInicio, columnaInicio);
		return false;

	}

	/**
	 * Metodo que se encarga de verificar si un objeto tiene relacion con un
	 * atributo o con un metodo
	 * 
	 * @return true si esto pasa, false en caso contrario
	 */
	public boolean esPuntoPropio() {

		if (caracterActual != '�') {
			return false;
		}
		System.out.println("Entra");
		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;

		almacenarSimbolo(lexema, Categoria.PUNTO_PROPIO, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();

		return true;

	}

	/**
	 * metodo que identifica si es fin de sentencia
	 * 
	 * @return true si es fin de sentencia, false caso contrario
	 */

	public boolean esFinDeSentencia() {
		if (caracterActual != ',') {
			return false;
		}
		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();
		almacenarSimbolo(lexema, Categoria.COMA_SENT_FIN_PROPIA, filaInicio, columnaInicio);
		return true;
	}

	/**
	 * Metodo para encontrar un operador de asignacion en el codigo
	 *
	 * 
	 * @return true si es operador de asignacion, false caso contrario
	 */
	public boolean esOperadorAsignacionPropia() {

		if (!(caracterActual == '?' || caracterActual == '�' || caracterActual == '|' || caracterActual == '!'
				|| caracterActual == '�' || caracterActual == '=')) {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		// char c = caracterActual;
		String lexema = "";
		lexema += caracterActual;

		if (caracterActual != '=') {
			obtenerSiguienteCaracter();

			if (caracterActual == '=') {
				lexema += caracterActual;
				almacenarSimbolo(lexema, Categoria.OPERADOR_ASIGNACION_PROPIA, filaInicio, columnaInicio);
				obtenerSiguienteCaracter();
				return true;
			} else {
				hacerBacktracking(posInicio, filaInicio, columnaInicio);
				return false;
			}
		}
		almacenarSimbolo(lexema, Categoria.OPERADOR_ASIGNACION_PROPIA, filaInicio, columnaInicio);
		obtenerSiguienteCaracter();
		return true;
	}

	/**
	 * metodo que determina si es incremento
	 * 
	 * @return true si el metodo es incremento , false caso contrario
	 */
	public boolean esIncrementoPropio() {

		if (caracterActual != '?') {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual == '?') {
			lexema += caracterActual;
			almacenarSimbolo(lexema, Categoria.INCREMENTO_PROPIO, filaInicio, columnaInicio);
			obtenerSiguienteCaracter();
			return true;
		} else {
			hacerBacktracking(posInicio, filaInicio, columnaInicio);
			return false;
		}

	}

	/**
	 * metodo que determina si es un agrupador que abre
	 * 
	 * @return true si es un agrupador abierto, false caso contrario
	 */

	public boolean esAgrupadorAbiertoPropio() {

		if (!(caracterActual == '(' || caracterActual == '{')) {
			return false;

		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";

		if (caracterActual == '{') {
			lexema += caracterActual;
			obtenerSiguienteCaracter();

			if (caracterActual == '.') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.LLAVE_IZQ, filaInicio, columnaInicio);
				return true;
			} else {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}

		} else if (caracterActual == '(') {
			lexema += caracterActual;
			obtenerSiguienteCaracter();

			if (caracterActual == '.') {
				lexema += caracterActual;
				obtenerSiguienteCaracter();
				almacenarSimbolo(lexema, Categoria.PARENTESIS_IZQ, filaInicio, columnaInicio);
				return true;
			} else {
				almacenarError(lexema, filaInicio, columnaInicio);
				return false;
			}
		}

		return true;

	}

	/**
	 * metodo que determina si es un agrupador que cierra
	 * 
	 * @return true si es un agrupador cerrado, false caso contrario
	 */

	public boolean esAgrupadorCerradoPropio() {

		if (!(caracterActual == '.')) {
			return false;

		}

		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual == '}') {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
			almacenarSimbolo(lexema, Categoria.LLAVE_DER, filaInicio, columnaInicio);
			return true;

		} else if (caracterActual == ')') {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
			almacenarSimbolo(lexema, Categoria.PARENTESIS_DER, filaInicio, columnaInicio);
			return true;
		} else {
			almacenarError(lexema, filaInicio, columnaInicio);
			return false;
		}

	}

	/**
	 * metodo que determina si es un caracter de separacion
	 * 
	 */
	public boolean esSeparadorPropio() {
		if (caracterActual != ';') {
			return false;
		}
		int filaInicio = filaActual, columnaInicio = columnaActual;
		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();
		almacenarSimbolo(lexema, Categoria.SEPARADOR_PROPIO, filaInicio, columnaInicio);
		return true;
	}

	/**
	 * metodo que determina si es decremento
	 * 
	 * @return true si el metodo es decremento , false caso contrario
	 */

	public boolean esDecrementoPropio() {

		if (!(caracterActual == '^')) {
			return false;
		}

		int posInicio = posicionActual, filaInicio = filaActual, columnaInicio = columnaActual;

		String lexema = "";
		lexema += caracterActual;
		obtenerSiguienteCaracter();

		if (caracterActual == '^') {
			lexema += caracterActual;
			obtenerSiguienteCaracter();
			almacenarSimbolo(lexema, Categoria.DECREMENTO_PROPIO, filaInicio, columnaInicio);

			return true;
		} else {
			hacerBacktracking(posInicio, filaInicio, columnaInicio);
			return false;
		}

	}

	/**
	 * modifica la tabla de simbolos
	 * 
	 * @return the tablaSimbolos
	 */
	public ArrayList<Token> getTablaSimbolos() {
		return tablaSimbolos;
	}

	/**
	 * modifica la tabla de errores
	 * 
	 * @return the tablaErrores
	 */
	public ArrayList<Token> getTablaErrores() {
		return tablaErrores;
	}

	/**
	 * recibe, obtiene lo del codigo fuente
	 * 
	 * @return the codigoFuente
	 */
	public String getCodigoFuente() {
		return codigoFuente;
	}

	/**
	 * @param codigoFuente the codigoFuente to set
	 */
	public void setCodigoFuente(String codigoFuente) {
		this.codigoFuente = codigoFuente;
	}

}
