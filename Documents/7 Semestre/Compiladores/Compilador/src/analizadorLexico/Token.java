package analizadorLexico;

/**
 * Clase Token para mostrar de manera ordenada los lexemas extraidos del codigo
 * fuente
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 29/09/2018
 *
 */
public class Token {

	private String lexema;
	private int fila, columna;
	private Categoria categoria;

	/**
	 * Metodo constructor para la inicialziacion de los atributos de la clase
	 * 
	 * @param lexema    Es la cadena de caracteres que se va obteniendo del codigo
	 *                  fuente
	 * @param fila      En que fila se encuentar ubicado el token
	 * @param columna   En que columna se encuentra ubicado el token
	 * @param categoria Nos determina de que tipo es el token
	 */
	public Token(String lexema, Categoria categoria, int fila, int columna) {
		this.lexema = lexema;
		this.categoria = categoria;
		this.fila = fila;
		this.columna = columna;
	}

	/**
	 * Metodo para traducir los tokens
	 * 
	 * @return
	 */
	public String getJavaCode() {
		String code = "";
		if (categoria == Categoria.CADENA_CARACTERES_PROPIA) {
			code = lexema.replaceAll("<", "\"");
			code = code.replaceAll(">", "\"");
		} else if (categoria == Categoria.OPERADOR_ASIGNACION_PROPIA) {
			if (lexema.equals("?=")) {
				code = "+=";
			} else if (lexema.equals("�=")) {
				code = "-=";
			} else if (lexema.equals("!=")) {
				code = "/=";
			} else if (lexema.equals("�=")) {
				code = "%=";
			} else if (lexema.equals("|=")) {
				code = "*=";
			} else {
				code = "=";
			}
		} else if (categoria == Categoria.IDENTIFICADOR_PROPIO) {
			code = lexema.replaceAll("_", "");
		} else if (categoria == Categoria.OPERADOR_ARITMETICO_PROPIO) {
			if (lexema.equals("?")) {
				code = "+";
			} else if (lexema.equals("�")) {
				code = "-";
			} else if (lexema.equals("!")) {
				code = "/";
			} else if (lexema.equals("�")) {
				code = "%";
			} else if (lexema.equals("|")) {
				code = "*";
			}
		} else if (categoria == Categoria.OPERADOR_RELACIONAL) {
			if (lexema.equals("�~")) {
				code = "<=";
			}
			if (lexema.equals("�~")) {
				code = ">=";
			}
			if (lexema.equals("~~")) {
				code = "==";
			}
		} else if (categoria == Categoria.PARENTESIS_IZQ) {
			code = "(";
		} else if (categoria == Categoria.PARENTESIS_DER) {
			code = ")";
		}

		if (code.equals(""))
			return lexema;
		else {
			return code;
		}
	}

	/**
	 * Obtiene la cadena de caracteres obtenida del codigo fuente
	 * 
	 * @return the lexema
	 */
	public String getLexema() {
		return lexema;
	}

	/**
	 * obtiene la fila donde se encuentra el Token
	 * 
	 * @return the fila
	 */
	public int getFila() {
		return fila;
	}

	/**
	 * Obitene la columna donde se encuentra el Token
	 * 
	 * @return the columna
	 */
	public int getColumna() {
		return columna;
	}

	/**
	 * Obtiene la categoria a la que pertenece el token
	 * 
	 * @return the categoria
	 */
	public Categoria getCategoria() {
		return categoria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return lexema + "";
	}

}
