import interfazGrafica.InterfazPrincipal;

/**
 * Clase para ejecutar la aplicacion
 * 
 * @author Felipe Arce Giraldo, Santiago Escudero, Daniel Delgado
 * @version 1.0 05/09/2018
 *
 */
public class App {

	public static void main(String[] args) {
		InterfazPrincipal ip = new InterfazPrincipal();
		ip.setVisible(true);

	}

}